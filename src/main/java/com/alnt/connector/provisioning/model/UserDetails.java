package com.alnt.connector.provisioning.model;

import java.io.Serializable;
import java.util.List;

public class UserDetails implements Serializable {

	private static final long serialVersionUID = -6507575691173675174L;

	private String UserName;
	private String Password;
	private String FirstName;
	private String LastName;
	private String Email;
	private String PhoneNumber;
	private String DateFormat;
	private String TimeFormat;
	private List<String> PlantCodes;
	private String DefaultPlant;
	private String[] UserGroups;
	private Boolean Enabled = Boolean.FALSE;
	private Boolean IsLocalAccount = Boolean.FALSE;
	private Boolean IsAdmin = Boolean.FALSE;
	private Boolean IsPlantAdministrator = Boolean.FALSE;
	private Boolean IsReportAdministrator = Boolean.FALSE;
	private Boolean IsConfigurator = Boolean.FALSE;
	private Boolean IsAlertAdministrator = Boolean.FALSE;
	private Boolean IsUser = Boolean.FALSE;
	private Boolean IsDocumentAdministrator = Boolean.FALSE;

	public String getUserName() {
		return UserName;
	}

	public void setUserName(String userName) {
		UserName = userName;
	}

	public String getPassword() {
		return Password;
	}

	public void setPassword(String password) {
		Password = password;
	}

	public String getFirstName() {
		return FirstName;
	}

	public void setFirstName(String firstName) {
		FirstName = firstName;
	}

	public String getLastName() {
		return LastName;
	}

	public void setLastName(String lastName) {
		LastName = lastName;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		Email = email;
	}

	public String getPhoneNumber() {
		return PhoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		PhoneNumber = phoneNumber;
	}

	public String getDateFormat() {
		return DateFormat;
	}

	public void setDateFormat(String dateFormat) {
		DateFormat = dateFormat;
	}

	public String getTimeFormat() {
		return TimeFormat;
	}

	public void setTimeFormat(String timeFormat) {
		TimeFormat = timeFormat;
	}

	public List<String> getPlantCodes() {
		return PlantCodes;
	}

	public void setPlantCodes(List<String> plantCodes) {
		PlantCodes = plantCodes;
	}

	public String getDefaultPlant() {
		return DefaultPlant;
	}

	public void setDefaultPlant(String defaultPlant) {
		DefaultPlant = defaultPlant;
	}

	public String[] getUserGroups() {
		return UserGroups;
	}

	public void setUserGroups(String[] userGroups) {
		UserGroups = userGroups;
	}

	public Boolean getEnabled() {
		return Enabled;
	}

	public void setEnabled(Boolean enabled) {
		Enabled = enabled;
	}

	public Boolean getIsLocalAccount() {
		return IsLocalAccount;
	}

	public void setIsLocalAccount(Boolean isLocalAccount) {
		IsLocalAccount = isLocalAccount;
	}

	public Boolean getIsAdmin() {
		return IsAdmin;
	}

	public void setIsAdmin(Boolean isAdmin) {
		IsAdmin = isAdmin;
	}

	public Boolean getIsPlantAdministrator() {
		return IsPlantAdministrator;
	}

	public void setIsPlantAdministrator(Boolean isPlantAdministrator) {
		IsPlantAdministrator = isPlantAdministrator;
	}

	public Boolean getIsReportAdministrator() {
		return IsReportAdministrator;
	}

	public void setIsReportAdministrator(Boolean isReportAdministrator) {
		IsReportAdministrator = isReportAdministrator;
	}

	public Boolean getIsConfigurator() {
		return IsConfigurator;
	}

	public void setIsConfigurator(Boolean isConfigurator) {
		IsConfigurator = isConfigurator;
	}

	public Boolean getIsAlertAdministrator() {
		return IsAlertAdministrator;
	}

	public void setIsAlertAdministrator(Boolean isAlertAdministrator) {
		IsAlertAdministrator = isAlertAdministrator;
	}

	public Boolean getIsUser() {
		return IsUser;
	}

	public void setIsUser(Boolean isUser) {
		IsUser = isUser;
	}

	public Boolean getIsDocumentAdministrator() {
		return IsDocumentAdministrator;
	}

	public void setIsDocumentAdministrator(Boolean isDocumentAdministrator) {
		IsDocumentAdministrator = isDocumentAdministrator;
	}

}
