package com.alnt.connector.provisioning.services;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.httpclient.HttpStatus;
import org.apache.log4j.Logger;
import org.json.JSONArray;

import com.alnt.access.common.constants.IProvisoiningConstants;
import com.alnt.access.provisioning.model.IProvisioningAttributes;
import com.alnt.access.provisioning.model.IProvisioningResult;
import com.alnt.access.provisioning.model.IProvisioningStatus;
import com.alnt.access.provisioning.model.IRoleAuditInfo;
import com.alnt.access.provisioning.model.ISystemInformation;
import com.alnt.access.provisioning.model.ProvisioningAttributes;
import com.alnt.access.provisioning.model.SystemInformation;
import com.alnt.access.provisioning.services.IConnectionInterface;
import com.alnt.access.provisioning.utils.ConnectorUtil;
import com.alnt.connector.constants.SyntempoConnectorConstants;
import com.alnt.connector.exception.SyntempoConnectorException;
import com.alnt.connector.provisioning.helper.Partition;
import com.alnt.connector.provisioning.helper.PasswordEncryptHelper;
import com.alnt.connector.provisioning.helper.SyntempoClientHelper;
import com.alnt.connector.provisioning.helper.SyntempoURLConnection;
import com.alnt.connector.provisioning.model.ProvisioningResult;
import com.alnt.connector.provisioning.model.RoleAuditInfo;
import com.alnt.connector.provisioning.model.RoleInformation;
import com.alnt.connector.provisioning.model.UserDetails;
import com.alnt.connector.provisioning.model.UserInformation;
import com.alnt.extractionconnector.common.constants.IExtractionConstants;
import com.alnt.extractionconnector.common.constants.IExtractionConstants.USER_ENTITLEMENT_KEY;
import com.alnt.extractionconnector.common.model.IUserInformation;
import com.alnt.extractionconnector.common.service.IExtractionInterface;
import com.alnt.extractionconnector.common.service.ISearchCallback;
import com.alnt.extractionconnector.exception.ExtractorConnectionException;
import com.alnt.extractionconnector.user.model.ExtractorAttributes;
import com.alnt.fabric.component.rolemanagement.search.IRoleInformation;
import com.alntgoogle.gson.Gson;
import com.alntgoogle.gson.reflect.TypeToken;
import com.alntgoogle.gson.FieldNamingPolicy;
import com.alntgoogle.gson.GsonBuilder;
import com.alntgoogle.gson.JsonElement;

/**
 * 
 * @author soori
 *
 */
public class SyntempoConnectionInterface implements IConnectionInterface, IExtractionInterface {

	private static final String CLASS_NAME = "com.alnt.connector.provisioning.services.SyntempoConnectionInterface";
	private static Logger logger = Logger.getLogger(CLASS_NAME);
	public String _baseUrl = null;
	public String _apiUserName = null;
	public String _apiPassword = null;
	public String _testUser = "";
	public String _jwtKey = null;
	public String _keytoEncryptUserPassword = null;
	private String externalUserIdAttribute = null;
	private boolean _showProvisioningWarnings = true;
	protected Set<String> excludeLogAttrList = null;

	public String _issuer = SyntempoConnectorConstants.JWT_ISSUER;
	public String _audience = SyntempoConnectorConstants.JWT_AUDIENCE;

	public SyntempoConnectionInterface(Map<String, String> connectionParams) throws SyntempoConnectorException {
		try {
			_baseUrl = (String) connectionParams.get(SyntempoConnectorConstants.BASE_URL);
			_apiUserName = (String) connectionParams.get(SyntempoConnectorConstants.API_USER_NAME);
			_apiPassword = (String) connectionParams.get(SyntempoConnectorConstants.API_PASSWORD);
			_jwtKey = (String) connectionParams.get(SyntempoConnectorConstants.JWT_KEY);
			_keytoEncryptUserPassword = (String) connectionParams.get(SyntempoConnectorConstants.KEY_TO_ENCRYPT_USERPASSWORD);
			if (connectionParams.containsKey(SyntempoConnectorConstants.SHOW_PROVISION_WARNINGS)) {
				_showProvisioningWarnings = Boolean.parseBoolean(connectionParams.get(SyntempoConnectorConstants.SHOW_PROVISION_WARNINGS));
			}
			if (connectionParams.containsKey(SyntempoConnectorConstants.TEST_USER)) {
				if (connectionParams.get(SyntempoConnectorConstants.TEST_USER) != null && !connectionParams.get(SyntempoConnectorConstants.TEST_USER).toString().isEmpty()) {
					_testUser = connectionParams.get(SyntempoConnectorConstants.TEST_USER);
				}
			}
			if (connectionParams.containsKey(SyntempoConnectorConstants.CONN_PARAM_JWT_ISSUER)) {
				if (connectionParams.get(SyntempoConnectorConstants.CONN_PARAM_JWT_ISSUER) != null && !connectionParams.get(SyntempoConnectorConstants.CONN_PARAM_JWT_ISSUER).toString().isEmpty()) {
					_issuer = connectionParams.get(SyntempoConnectorConstants.CONN_PARAM_JWT_ISSUER);
				}
			}
			if (connectionParams.containsKey(SyntempoConnectorConstants.CONN_PARAM_JWT_AUDIENCE)) {
				if (connectionParams.get(SyntempoConnectorConstants.CONN_PARAM_JWT_AUDIENCE) != null && !connectionParams.get(SyntempoConnectorConstants.CONN_PARAM_JWT_AUDIENCE).toString().isEmpty()) {
					_audience = connectionParams.get(SyntempoConnectorConstants.CONN_PARAM_JWT_AUDIENCE);
				}
			}
			externalUserIdAttribute = SyntempoConnectorConstants.SYNTEMPO_USERID;
			String excludeAttributes = connectionParams.get(SyntempoConnectorConstants.SENSITIVE_ATTRIBUTES);
			if (excludeAttributes != null && !excludeAttributes.isEmpty()) {
				excludeLogAttrList = ConnectorUtil.convertStringToList(excludeAttributes, ",");
			}
			if (excludeLogAttrList == null) {
				excludeLogAttrList = new HashSet<String>();
				excludeLogAttrList.add(SyntempoConnectorConstants.ATTR_USER_PASSWORD);
			}
		} catch (Exception _e) {
			throw new SyntempoConnectorException("connection parameters should be not be NULL");
		}
	}

	// RECON OPERATIONS :: BEGIN

	/**
	 * 
	 * soori - 2019-12-06 - 12:41:01 pm
	 * 
	 * @param options
	 * @param fetchSize
	 * @param searchCriteria
	 * @param callback
	 * @throws ExtractorConnectionException
	 * @see com.alnt.extractionconnector.role.services.IRoleExtractionConnectionInterface#getAllRoles(java.util.Map, int, java.util.Map, com.alnt.extractionconnector.common.service.ISearchCallback)
	 *
	 *
	 */
	@Override
	public void getAllRoles(Map<String, List<ExtractorAttributes>> options, int fetchSize, Map<String, String> searchCriteria, ISearchCallback callback) throws ExtractorConnectionException {
		try {
			logger.debug(CLASS_NAME + " Start of getAllRoles(),  Begin");
			String[] roles = null;
			List<IRoleInformation> rolesList = new ArrayList<IRoleInformation>();
			String endPoint = _baseUrl.concat(SyntempoConnectorConstants.ENDPOINT_ROLES);
			SyntempoURLConnection rs2Connection = SyntempoClientHelper.getConnection(endPoint, SyntempoConnectorConstants.REQ_METHOD_TYPE_GET, _apiUserName, _apiPassword, _jwtKey, null, _issuer, _audience);
			int serverResponse = rs2Connection.getResponseCode();
			logger.debug(CLASS_NAME + " http response code,  " + serverResponse);
			if (serverResponse == HttpStatus.SC_OK) {
				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader((rs2Connection.getInputStream())));
				String output = bufferedReader.readLine();
				if (output != null) {
					JSONArray restapiresponseArray = new JSONArray(output);
					if (null != restapiresponseArray && restapiresponseArray.length() > 0) {
						roles = new Gson().fromJson(String.valueOf(restapiresponseArray), String[].class);
					}
				}
			}
			for (String role : roles) {
				logger.debug(CLASS_NAME + " getAllRoles(): Extract role name is :: " + role);
				String roleName = role;
				String roleDesc = role;
				IRoleInformation roleInformation = getRoleInformation(options, roleName, roleDesc);
				rolesList.add(roleInformation);
			}
			callback.processSearchResult(rolesList);
			logger.debug(CLASS_NAME + " getAllRoles(): End of getAllRoles(), ");
		} catch (Exception e) {
			logger.error(CLASS_NAME + " getRoles() : execution failed ", e);
		}
	}

	@Override
	public void getIncrementalRoles(Date lastRunDate, Map<String, List<ExtractorAttributes>> options, int fetchSize, Map<String, String> searchCriteria, ISearchCallback callback) throws ExtractorConnectionException {
	}

	/**
	 * 
	 * soori - 2019-12-06 - 12:33:13 pm
	 * 
	 * @param options
	 * @param fetchSize
	 * @param searchCriteria
	 * @param callback
	 * @throws ExtractorConnectionException
	 * @see com.alnt.extractionconnector.user.services.IUserExtractionConnectionInterface#getAllUsersWithCallback(java.util.Map, int, java.util.Map, com.alnt.extractionconnector.common.service.ISearchCallback)
	 *
	 *
	 */
	@Override
	public void getAllUsersWithCallback(Map<String, List<ExtractorAttributes>> options, int fetchSize, Map<String, String> searchCriteria, ISearchCallback callback) throws ExtractorConnectionException {
		try {
			logger.debug(CLASS_NAME + " getAllUsersWithCallback(): Start of getAllUsersWithCallback method");
			long start = System.currentTimeMillis();
			int totalUsers = 0;
			OutputStream outputStream = null;
			List<IUserInformation> userInfoList = new ArrayList<IUserInformation>();
			List<UserDetails> users = new ArrayList<UserDetails>();
			String gsonString = "[]";
			String endPoint = _baseUrl.concat(SyntempoConnectorConstants.ENDPOINT_USERS);
			logger.debug(CLASS_NAME + " getAllUsersWithCallback(): endPoint " + endPoint);
			SyntempoURLConnection rs2Connection = SyntempoClientHelper.getConnection(endPoint, SyntempoConnectorConstants.REQ_METHOD_TYPE_POST, _apiUserName, _apiPassword, _jwtKey, null, _issuer, _audience);
			// logger.debug(CLASS_NAME + " create() request payload ::: " + gsonString);
			outputStream = rs2Connection.getOutputStream();
			outputStream.write(gsonString.getBytes());
			outputStream.flush();
			int serverResponse = rs2Connection.getResponseCode();
			logger.debug(CLASS_NAME + " http response code,  " + serverResponse);
			if (serverResponse != HttpStatus.SC_OK) {
				logger.error(CLASS_NAME + " getAllUsersWithCallback()  Error While fetching Users " + rs2Connection.getResponseMessage());
			} else {
				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader((rs2Connection.getInputStream())));
				String output = bufferedReader.readLine();
				if (output != null) {
					JSONArray restapiresponseArray = new JSONArray(output);
					if (null != restapiresponseArray && restapiresponseArray.length() > 0) {

						Type listType = new TypeToken<List<UserDetails>>() {
						}.getType();
						users = new Gson().fromJson(String.valueOf(restapiresponseArray), listType);
					}
				}
			}
			totalUsers = totalUsers + users.size();
			if (null != users && !users.isEmpty()) {
				for (UserDetails user : users) {
					IUserInformation userInfo = processExternalUserInfo(user, options, null);
					userInfoList.add(userInfo);
				}
				if (fetchSize > 0) {
					List<List<IUserInformation>> data = (Partition.ofSize(userInfoList, fetchSize));
					for (List<IUserInformation> chunk : data) {
						callback.processSearchResult(chunk);
					}
				} else {
					callback.processSearchResult(userInfoList);
				}
			}
			long end = System.currentTimeMillis();
			float msec = end - start;
			float sec = msec / 1000F;
			float minutes = sec / 60F;
			logger.debug(CLASS_NAME + " getAllUsersWithCallback(): total time taken for  " + totalUsers + "  User Recon   is " + minutes + "  minutes");
		} catch (Exception _ex) {
			logger.error(CLASS_NAME + " getAllUsersWithCallback()  Exception While fetching Users " + _ex.getLocalizedMessage());
		}
	}

	@Override
	public void getIncrementalUsersWithCallback(Date lastRunDate, Map<String, List<ExtractorAttributes>> options, final int fetchSize, Map<String, String> searchCriteria, ISearchCallback callback) throws ExtractorConnectionException {
	}

	/**
	 * 
	 * soori - 2019-12-06 - 12:28:29 pm
	 * 
	 * @param cardHolder
	 * @param options
	 * @return IUserInformation
	 *
	 */
	private IUserInformation processExternalUserInfo(UserDetails userDetails, Map<String, List<ExtractorAttributes>> options, String lastRunAt) {
		IUserInformation userInformation = new UserInformation();
		Map<String, Map<String, List<Map<String, Object>>>> allEntitlements = new HashMap<String, Map<String, List<Map<String, Object>>>>();
		Map<String, Object> userAttr = new HashMap<String, Object>();
		if (null != userDetails) {
			userAttr = convertFormat(SyntempoConnectorConstants.ATTR_USER_USERNAME, userDetails.getUserName(), options, userAttr, "User");
			userAttr = convertFormat(SyntempoConnectorConstants.ATTR_USER_FN, userDetails.getFirstName(), options, userAttr, "User");
			userAttr = convertFormat(SyntempoConnectorConstants.ATTR_USER_LN, userDetails.getLastName(), options, userAttr, "User");
			userAttr = convertFormat(SyntempoConnectorConstants.ATTR_USER_EMAIL, userDetails.getEmail(), options, userAttr, "User");
			userAttr = convertFormat(SyntempoConnectorConstants.ATTR_USER_PHONE, userDetails.getPhoneNumber(), options, userAttr, "User");
			userAttr = convertFormat(SyntempoConnectorConstants.ATTR_USER_DATEFORMAT, userDetails.getDateFormat(), options, userAttr, "User");
			userAttr = convertFormat(SyntempoConnectorConstants.ATTR_USER_TIMEFORMAT, userDetails.getTimeFormat(), options, userAttr, "User");
			userAttr = convertFormat(SyntempoConnectorConstants.ATTR_USER_DEFAULTPLANT, userDetails.getDefaultPlant(), options, userAttr, "User");
			userAttr = convertFormat(SyntempoConnectorConstants.ATTR_USER_ENABLED, userDetails.getEnabled(), options, userAttr, "User");
			userAttr = convertFormat(SyntempoConnectorConstants.ATTR_USER_ISLOCALACCOUNT, userDetails.getIsLocalAccount(), options, userAttr, "User");
			userAttr = convertFormat(SyntempoConnectorConstants.ATTR_USER_ISADMIN, userDetails.getIsAdmin(), options, userAttr, "User");
			userAttr = convertFormat(SyntempoConnectorConstants.ATTR_USER_ISPLANTADMIN, userDetails.getIsPlantAdministrator(), options, userAttr, "User");
			userAttr = convertFormat(SyntempoConnectorConstants.ATTR_USER_ISREPORTADMIN, userDetails.getIsReportAdministrator(), options, userAttr, "User");
			userAttr = convertFormat(SyntempoConnectorConstants.ATTR_USER_ISCONFIGURATOR, userDetails.getIsConfigurator(), options, userAttr, "User");
			userAttr = convertFormat(SyntempoConnectorConstants.ATTR_USER_ISALERTADMIN, userDetails.getIsAlertAdministrator(), options, userAttr, "User");
			userAttr = convertFormat(SyntempoConnectorConstants.ATTR_USER_ISUSER, userDetails.getIsUser(), options, userAttr, "User");
			userAttr = convertFormat(SyntempoConnectorConstants.ATTR_USER_ISDOCADMIN, userDetails.getIsDocumentAdministrator(), options, userAttr, "User");
		   //List<String> PlantCodes;

			List<Map<String, Object>> rolesList = new ArrayList<Map<String, Object>>();
			if (null != userDetails.getUserGroups() && userDetails.getUserGroups().length > 0) {
				for (String userRole : userDetails.getUserGroups()) {
					logger.debug(CLASS_NAME + " processExternalUserInfo(): start processing user role information");
					Map<String, Object> processedDataMap = new HashMap<String, Object>();
					processedDataMap = convertFormat(SyntempoConnectorConstants.ATTR_ROLE_ID, userRole, options, processedDataMap, "Role");
					rolesList.add(processedDataMap);
					logger.debug(CLASS_NAME + " processExternalUserInfo(): finished user role information processing");
				}
			}
			Map<String, List<Map<String, Object>>> entitlement = new HashMap<String, List<Map<String, Object>>>();
			if (rolesList != null && rolesList.size() > 0) {
				entitlement.put(USER_ENTITLEMENT_KEY.ROLES.toString(), rolesList);
			}
			if (!entitlement.isEmpty()) {
				allEntitlements.put(userDetails.getUserName(), entitlement);
				logger.trace(CLASS_NAME + " processExternalUserInfo(): finished user allEntitlements processing  :: " + allEntitlements);
			}
			// SYNTEMPO USER GROUPS NEVER EMPTY
			/*
			 * else { logger.trace(CLASS_NAME + " processExternalUserInfo(): no entitlements found for the user  ::  " + userDetails.getUserName()); }
			 */
			userInformation.setEntitlements(allEntitlements);
			userInformation.setUserDetails(userAttr);
		}
		return userInformation;
	}

	/**
	 * 
	 * soori - 2019-12-06 - 12:28:18 pm
	 * 
	 * @param fieldName
	 * @param fieldValue
	 * @param options
	 * @param userAttr
	 * @param type
	 * @return
	 *
	 */
	private Map<String, Object> convertFormat(String fieldName, Object fieldValue, Map<String, List<ExtractorAttributes>> options, Map<String, Object> userAttr, String type) {
		if (options != null && options.size() > 0 && options.containsKey(fieldName)) {
			List<ExtractorAttributes> extractorAttributesList = options.get(fieldName);
			if (extractorAttributesList != null && extractorAttributesList.size() > 0) {
				for (ExtractorAttributes extractorAttribute : extractorAttributesList) {
					boolean typeMatch = (type.equals("User") && extractorAttribute.isUserAttr()) || (type.equals("Role") && extractorAttribute.isRoleAttr());
					if (extractorAttribute != null && typeMatch) {
						logger.debug(CLASS_NAME + " processOutMap() : setting value for: " + extractorAttribute.getAttributeName());
						userAttr.put(extractorAttribute.getAttributeName(), fieldValue != null ? fieldValue.toString() : null);
					}
				}
			}
		}
		return userAttr;
	}

	/**
	 * 
	 * soori - 2019-12-06 - 12:34:22 pm
	 * 
	 * @param options
	 * @param roleId
	 * @param roleDesc
	 * @return IRoleInformation
	 *
	 */
	private IRoleInformation getRoleInformation(Map<String, List<ExtractorAttributes>> options, String roleId, String roleDesc) {
		IRoleInformation roleInformation = new RoleInformation();
		logger.trace(CLASS_NAME + " getRoleInformation(): start processing role name " + roleDesc);
		// XXX:if plant code needs to be included in role info
		roleInformation.setRoleType(IExtractionConstants.ROLE_TYPE_SINGLE);
		roleInformation.setName(roleDesc);
		roleInformation.setDescription(roleDesc);
		roleInformation.setValidFrom(null);
		roleInformation.setValidTo(null);
		logger.trace(CLASS_NAME + " getRoleInformation(): finish processing role name " + roleDesc);
		return roleInformation;
	}

	@Override
	public List<IUserInformation> getIncrementalUsers(Date arg0, Map<String, List<ExtractorAttributes>> arg1, int arg2, int arg3, Map<String, String> arg4) throws ExtractorConnectionException {
		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Map getUsers(Map arg0, List arg1) throws ExtractorConnectionException {
		return null;
	}

	@Override
	public void getUsers(String arg0, ISearchCallback arg1) throws ExtractorConnectionException {
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void invokeUsers(Map arg0, List arg1) throws ExtractorConnectionException {
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List getAllRoles(String arg0) throws ExtractorConnectionException {
		return null;
	}

	@Override
	public List<com.alnt.extractionconnector.common.model.IRoleInformation> getAllRoles(Map<String, List<ExtractorAttributes>> arg0, int arg1, int arg2, Map<String, String> arg3) throws ExtractorConnectionException {
		return null;
	}

	@Override
	public List<com.alnt.extractionconnector.common.model.IRoleInformation> getIncrementalRoles(Date arg0, Map<String, List<ExtractorAttributes>> arg1, int arg2, int arg3, Map<String, String> arg4) throws ExtractorConnectionException {
		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List getRoles(String arg0) throws ExtractorConnectionException {
		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List getRolesForUser(String arg0) throws ExtractorConnectionException {
		return null;
	}

	@Override
	public void searchRoles(String arg0, ISearchCallback arg1) throws ExtractorConnectionException {
	}

	@Override
	public boolean supportsProvisioning() {
		return false;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Map getAllUsers(Map arg0, List arg1) throws ExtractorConnectionException {
		return null;
	}

	@Override
	public List<IUserInformation> getAllUsers(Map<String, List<ExtractorAttributes>> arg0, int arg1, int arg2, Map<String, String> arg3) throws ExtractorConnectionException {
		return null;
	}
	// RECON OPERATIONS :: END

	// PROVISIONING OPERATIONS :: BEGIN
	/**
	 * 
	 * soori 2019-12-02 - 5:36:27 pm
	 * 
	 * @return
	 * @throws Exception
	 * @see com.alnt.access.provisioning.services.IConnectionInterface#testConnection()
	 *
	 *
	 */
	@Override
	public boolean testConnection() throws Exception {
		logger.info(CLASS_NAME + " testConnection(): Start of testConnection method");
		boolean testConnectionPass = false;
		try {
			String queryString = SyntempoConnectorConstants.QUERY_STRING_USERNAME.concat(_testUser);
			String endPoint = _baseUrl.concat(SyntempoConnectorConstants.ENDPOINT_GET_USER);
			SyntempoURLConnection rs2Connection = SyntempoClientHelper.getConnection(endPoint, SyntempoConnectorConstants.REQ_METHOD_TYPE_GET, _apiUserName, _apiPassword, _jwtKey, queryString, _issuer, _audience);
			if (_testUser.equals("")) {
				if (rs2Connection.getResponseCode() == 409) {
					logger.debug("Test connection success , " + rs2Connection.getResponseMessage());
					testConnectionPass = true;
				}
			} else {
				if (rs2Connection.getResponseCode() == 200) {
					logger.debug("Test connection success , " + rs2Connection.getResponseMessage());
					testConnectionPass = true;
				}
			}
		} catch (Exception _ex) {
			logger.debug("Test connection failed , " + _ex.getLocalizedMessage());
			return false;
		}
		return testConnectionPass;
	}

	/**
	 * 
	 * soori - 9:57:14 pm
	 * 
	 * @return
	 * @throws Exception
	 * @see com.alnt.access.provisioning.services.IConnectionInterface#getAttributes()
	 */
	@Override
	public List<IProvisioningAttributes> getAttributes() throws Exception {
		List<IProvisioningAttributes> provAttributes = new ArrayList<IProvisioningAttributes>();
		// Mandatory Attributes
		provAttributes.add(new ProvisioningAttributes(SyntempoConnectorConstants.ATTR_USER_LN, SyntempoConnectorConstants.ATTR_USER_LN, true, SyntempoConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(SyntempoConnectorConstants.ATTR_USER_FN, SyntempoConnectorConstants.ATTR_USER_FN, true, SyntempoConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(SyntempoConnectorConstants.ATTR_USER_USERNAME, SyntempoConnectorConstants.ATTR_USER_USERNAME, true, SyntempoConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(SyntempoConnectorConstants.ATTR_USER_PASSWORD, SyntempoConnectorConstants.ATTR_USER_PASSWORD, true, SyntempoConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(SyntempoConnectorConstants.ATTR_USER_DEFAULTPLANT, SyntempoConnectorConstants.ATTR_USER_DEFAULTPLANT, true, SyntempoConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(SyntempoConnectorConstants.ATTR_USER_PLANTCODES, SyntempoConnectorConstants.ATTR_USER_PLANTCODES, true, SyntempoConnectorConstants.ATTR_TYPE_LIST));
		provAttributes.add(new ProvisioningAttributes(SyntempoConnectorConstants.ATTR_USER_GROUPS, SyntempoConnectorConstants.ATTR_USER_GROUPS, true, SyntempoConnectorConstants.ATTR_TYPE_LIST));
		// Optional attributes
		provAttributes.add(new ProvisioningAttributes(SyntempoConnectorConstants.ATTR_USER_EMAIL, SyntempoConnectorConstants.ATTR_USER_EMAIL, false, SyntempoConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(SyntempoConnectorConstants.ATTR_USER_PHONE, SyntempoConnectorConstants.ATTR_USER_PHONE, false, SyntempoConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(SyntempoConnectorConstants.ATTR_USER_DATEFORMAT, SyntempoConnectorConstants.ATTR_USER_DATEFORMAT, false, SyntempoConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(SyntempoConnectorConstants.ATTR_USER_TIMEFORMAT, SyntempoConnectorConstants.ATTR_USER_TIMEFORMAT, false, SyntempoConnectorConstants.ATTR_TYPE_STRING));
		// XXX: should we make it mandatory ?(because we don't know that will it have some default value)
		provAttributes.add(new ProvisioningAttributes(SyntempoConnectorConstants.ATTR_USER_ENABLED, SyntempoConnectorConstants.ATTR_USER_ENABLED, false, SyntempoConnectorConstants.ATTR_TYPE_BOOLEAN));
		provAttributes.add(new ProvisioningAttributes(SyntempoConnectorConstants.ATTR_USER_ISLOCALACCOUNT, SyntempoConnectorConstants.ATTR_USER_ISLOCALACCOUNT, false, SyntempoConnectorConstants.ATTR_TYPE_BOOLEAN));
		provAttributes.add(new ProvisioningAttributes(SyntempoConnectorConstants.ATTR_USER_ISADMIN, SyntempoConnectorConstants.ATTR_USER_ISADMIN, false, SyntempoConnectorConstants.ATTR_TYPE_BOOLEAN));
		provAttributes.add(new ProvisioningAttributes(SyntempoConnectorConstants.ATTR_USER_ISUSER, SyntempoConnectorConstants.ATTR_USER_ISUSER, false, SyntempoConnectorConstants.ATTR_TYPE_BOOLEAN));
		provAttributes.add(new ProvisioningAttributes(SyntempoConnectorConstants.ATTR_USER_ISPLANTADMIN, SyntempoConnectorConstants.ATTR_USER_ISPLANTADMIN, false, SyntempoConnectorConstants.ATTR_TYPE_BOOLEAN));
		provAttributes.add(new ProvisioningAttributes(SyntempoConnectorConstants.ATTR_USER_ISREPORTADMIN, SyntempoConnectorConstants.ATTR_USER_ISREPORTADMIN, false, SyntempoConnectorConstants.ATTR_TYPE_BOOLEAN));
		provAttributes.add(new ProvisioningAttributes(SyntempoConnectorConstants.ATTR_USER_ISCONFIGURATOR, SyntempoConnectorConstants.ATTR_USER_ISCONFIGURATOR, false, SyntempoConnectorConstants.ATTR_TYPE_BOOLEAN));
		provAttributes.add(new ProvisioningAttributes(SyntempoConnectorConstants.ATTR_USER_ISALERTADMIN, SyntempoConnectorConstants.ATTR_USER_ISALERTADMIN, false, SyntempoConnectorConstants.ATTR_TYPE_BOOLEAN));
		provAttributes.add(new ProvisioningAttributes(SyntempoConnectorConstants.ATTR_USER_ISDOCADMIN, SyntempoConnectorConstants.ATTR_USER_ISDOCADMIN, false, SyntempoConnectorConstants.ATTR_TYPE_BOOLEAN));

		return provAttributes;
	}

	/**
	 * 
	 * soori -10-Dec-2019 - 5:19:17 pm
	 * 
	 * @param params
	 * @return
	 * @throws Exception
	 * @see com.alnt.access.provisioning.services.IConnectionInterface#getAttributes(java.util.Map)
	 *
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public List getAttributes(Map params) throws Exception {
		return null;
	}

	/**
	 * 
	 * soori - 8:23:42 pm
	 * 
	 * @param userId
	 * @return
	 * @throws Exception
	 * @see com.alnt.access.provisioning.services.IConnectionInterface#isUserProvisioned(java.lang.String)
	 */
	@Override
	public ISystemInformation isUserProvisioned(String userId) throws Exception {
		logger.debug(CLASS_NAME + " isUserProvisioned(): Checking isUserProvisioned  with  UserDetails Id  : " + userId);
		ISystemInformation systemInformation = new SystemInformation(false);
		UserDetails user = getUserByUserDetailsId(userId);
		if (null != user) {
			systemInformation.setProvisioned(true);
		}
		return systemInformation;
	}

	private UserDetails getUserByUserDetailsId(String userId) {
		try {
			String queryString = SyntempoConnectorConstants.QUERY_STRING_USERNAME.concat(userId);
			String endPoint = _baseUrl.concat(SyntempoConnectorConstants.ENDPOINT_GET_USER);
			SyntempoURLConnection rs2Connection = SyntempoClientHelper.getConnection(endPoint, SyntempoConnectorConstants.REQ_METHOD_TYPE_GET, _apiUserName, _apiPassword, _jwtKey, queryString, _issuer, _audience);
			int serverResponse = rs2Connection.getResponseCode();
			logger.debug(CLASS_NAME + " http response code,  " + serverResponse);
			if (serverResponse == HttpStatus.SC_OK) {
				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader((rs2Connection.getInputStream())));
				String output = bufferedReader.readLine();
				if (output != null) {
					return new Gson().fromJson(output, UserDetails.class);
				}
			}
			return null;
		} catch (Exception _ex) {
			return null;
		}
	}

	/**
	 * 
	 * soori - 9:55:29 pm
	 * 
	 * @param userId
	 * @return
	 * @throws Exception
	 * @see com.alnt.access.provisioning.services.IConnectionInterface#isUserLocked(java.lang.String)
	 */
	@Override
	public boolean isUserLocked(String userId) throws Exception {
		UserDetails user = getUserByUserDetailsId(userId);
		if (null != user && !user.getEnabled()) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * soori - 10:33:36 pm
	 * 
	 * @param requestNumber
	 * @param roles
	 * @param parameters
	 * @param requestDetails
	 * @param attMapping
	 * @return
	 * @throws Exception
	 * @see com.alnt.access.provisioning.services.IConnectionInterface#create(java.lang.Long, java.util.List, java.util.Map, java.util.List, java.util.Map)
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public IProvisioningResult create(Long requestNumber, List roles, Map parameters, List requestDetails, Map<String, String> attMapping) throws Exception {
		logger.debug(CLASS_NAME + " create(): Start of Create Method.");
		IProvisioningResult provResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CREATE_USER, IProvisoiningConstants.CREATE_USER_FAILURE, true, "User is not provisioned !!!");
		boolean userCreated = true;
		OutputStream outputStream = null;
		List<IRoleAuditInfo> roleAuditInfoList = new ArrayList<IRoleAuditInfo>();
		try {
			ConnectorUtil.logInputParamters(parameters, excludeLogAttrList, logger);
			UserDetails userTorCreate = null;
			if (parameters.get(SyntempoConnectorConstants.ATTR_USER_PLANTCODES) == null) {
				return prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CREATE_USER, IProvisoiningConstants.CREATE_USER_FAILURE, true, "PlantCodes does not passed!!");
			} else {
				String plantCodes = parameters.get(SyntempoConnectorConstants.ATTR_USER_PLANTCODES).toString();
				parameters.remove(SyntempoConnectorConstants.ATTR_USER_PLANTCODES);
				userTorCreate = prepareUserDetails(parameters);
				userTorCreate.setPlantCodes(Arrays.asList(plantCodes.split(";")));
			}
			if (parameters.get(SyntempoConnectorConstants.ATTR_USER_PASSWORD) != null && !parameters.get(SyntempoConnectorConstants.ATTR_USER_PASSWORD).toString().isEmpty()) {
				userTorCreate.setPassword(PasswordEncryptHelper.encrypt(_keytoEncryptUserPassword, parameters.get(SyntempoConnectorConstants.ATTR_USER_PASSWORD).toString()));
			}

			String[] userRoles = new String[0];
			if (roles != null && !roles.isEmpty()) {
				Iterator<IRoleInformation> addRoleItr = roles.iterator();
				while (addRoleItr.hasNext()) {
					while (addRoleItr.hasNext()) {
						ArrayList<String> rolesInSystemList = new ArrayList<String>(Arrays.asList(userRoles));
						IRoleInformation roleInformation = (IRoleInformation) addRoleItr.next();
						rolesInSystemList.add(roleInformation.getName());
						roleAuditInfoList.add(getRoleAuditInfo(roleInformation, IProvisoiningConstants.ADD_ROLE));
						userRoles = rolesInSystemList.toArray(userRoles);
					}
				}
				userTorCreate.setUserGroups(userRoles);
			} else {
				return prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CREATE_USER, IProvisoiningConstants.CREATE_USER_FAILURE, true, "User roles does not passed!!");
			}

			Gson gson = new Gson();
			String gsonString = gson.toJson(userTorCreate);
			String endPoint = _baseUrl.concat(SyntempoConnectorConstants.ENDPOINT_CREATE_UPDATE_USER);
			SyntempoURLConnection rs2Connection = SyntempoClientHelper.getConnection(endPoint, SyntempoConnectorConstants.REQ_METHOD_TYPE_POST, _apiUserName, _apiPassword, _jwtKey, null, _issuer, _audience);
			// logger.debug(CLASS_NAME + " create() request payload ::: " + gsonString);
			outputStream = rs2Connection.getOutputStream();
			outputStream.write(gsonString.getBytes());
			outputStream.flush();
			int serverResponse = rs2Connection.getResponseCode();
			logger.debug(CLASS_NAME + " http response code,  " + serverResponse);
			if (serverResponse != HttpStatus.SC_OK) {
				logger.error(CLASS_NAME + "   create()   " + rs2Connection.getResponseMessage());
				provResult.setMsgDesc(rs2Connection.getResponseMessage());
				return provResult;
			} else {
				provResult.setUserId(parameters.get(externalUserIdAttribute).toString());
				provResult.setUserCreated(userCreated);
				provResult.setMsgCode(IProvisoiningConstants.CREATE_USER_SUCCESS);
				provResult.setMsgDesc("User provisioned successfully");
				provResult.setProvFailed(false);
				parameters.put(externalUserIdAttribute, parameters.get(externalUserIdAttribute).toString());
				provResult.setRoleList(roleAuditInfoList);
			}
		} catch (Exception e) {
			logger.error(CLASS_NAME + "create()", e);
			provResult.setMsgDesc(e.getMessage());
			roleAuditInfoList = new ArrayList<IRoleAuditInfo>();
			provResult.setRoleList(roleAuditInfoList);
			return provResult;
		}
		logger.debug(CLASS_NAME + " create(): End of Create Method.");
		return provResult;
	}

	/**
	 * 
	 * @param userAttributesMap
	 * @return UserDetails
	 */
	@SuppressWarnings({ "rawtypes" })
	public UserDetails prepareUserDetails(Map userAttributesMap) {
		Gson gson = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE).create();
		JsonElement jsonElement = gson.toJsonTree(userAttributesMap);
		UserDetails userDetails = gson.fromJson(jsonElement, UserDetails.class);
		return userDetails;
	}

	/**
	 * 
	 * soori -10-Dec-2019 - 5:17:17 pm
	 * 
	 * updates first name , last name , member of all sites , status, dates , image(replace)
	 * 
	 * @param requestNumber
	 * @param roles
	 * @param parameters
	 * @param requestDetails
	 * @param attMapping
	 * @return
	 * @throws Exception
	 * @see com.alnt.access.provisioning.services.IConnectionInterface#update(java.lang.Long, java.util.List, java.util.Map, java.util.List, java.util.Map)
	 *
	 */
	@Override
	@SuppressWarnings("rawtypes")
	public IProvisioningResult update(Long requestNumber, List roles, Map parameters, List requestDetails, Map<String, String> attMapping) throws Exception {
		logger.info(CLASS_NAME + " update(): Start of update method");
		IProvisioningResult provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CHANGE_USER, IProvisoiningConstants.CHANGE_USER_FAILURE, true, "update user failed in target system!");
		OutputStream outputStream = null;
		try {

			// TODO:: check not null and update
			ConnectorUtil.logInputParamters(parameters, excludeLogAttrList, logger);
			if (parameters.get(externalUserIdAttribute) == null) {
				logger.debug(CLASS_NAME + " update() userId does not passed!!");
				provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CHANGE_USER, IProvisoiningConstants.CHANGE_USER_FAILURE, true, "userId does not passed!!");
			} else {
				String userIdToUpdate = parameters.get(externalUserIdAttribute).toString();
				UserDetails userToUpdate = getUserByUserDetailsId(userIdToUpdate);
				if (null != userToUpdate) {
					UserDetails userInRequest = null;
					if (parameters.get(SyntempoConnectorConstants.ATTR_USER_PLANTCODES) == null) {
						userInRequest = prepareUserDetails(parameters);
					} else {
						String plantCodes = parameters.get(SyntempoConnectorConstants.ATTR_USER_PLANTCODES).toString();
						parameters.remove(SyntempoConnectorConstants.ATTR_USER_PLANTCODES);
						userInRequest = prepareUserDetails(parameters);
						userToUpdate.setPlantCodes(Arrays.asList(plantCodes.split(";")));
					}

					if (parameters.get(SyntempoConnectorConstants.ATTR_USER_FN) != null && !parameters.get(SyntempoConnectorConstants.ATTR_USER_FN).toString().isEmpty()) {
						userToUpdate.setFirstName(userInRequest.getFirstName());
					}
					if (parameters.get(SyntempoConnectorConstants.ATTR_USER_PASSWORD) != null && !parameters.get(SyntempoConnectorConstants.ATTR_USER_PASSWORD).toString().isEmpty()) {
						userToUpdate.setPassword(PasswordEncryptHelper.encrypt(_keytoEncryptUserPassword, parameters.get(SyntempoConnectorConstants.ATTR_USER_PASSWORD).toString()));
					}
					if (parameters.get(SyntempoConnectorConstants.ATTR_USER_LN) != null && !parameters.get(SyntempoConnectorConstants.ATTR_USER_LN).toString().isEmpty()) {
						userToUpdate.setLastName(userInRequest.getLastName());
					}
					if (parameters.get(SyntempoConnectorConstants.ATTR_USER_EMAIL) != null && !parameters.get(SyntempoConnectorConstants.ATTR_USER_EMAIL).toString().isEmpty()) {
						userToUpdate.setEmail(userInRequest.getEmail());
					}
					if (parameters.get(SyntempoConnectorConstants.ATTR_USER_PHONE) != null && !parameters.get(SyntempoConnectorConstants.ATTR_USER_PHONE).toString().isEmpty()) {
						userToUpdate.setPhoneNumber(userInRequest.getPhoneNumber());
					}

					if (parameters.get(SyntempoConnectorConstants.ATTR_USER_DATEFORMAT) != null && !parameters.get(SyntempoConnectorConstants.ATTR_USER_DATEFORMAT).toString().isEmpty()) {
						userToUpdate.setDateFormat(userInRequest.getDateFormat());
					}
					if (parameters.get(SyntempoConnectorConstants.ATTR_USER_TIMEFORMAT) != null && !parameters.get(SyntempoConnectorConstants.ATTR_USER_TIMEFORMAT).toString().isEmpty()) {
						userToUpdate.setTimeFormat(userInRequest.getTimeFormat());
					}
					if (parameters.get(SyntempoConnectorConstants.ATTR_USER_DEFAULTPLANT) != null && !parameters.get(SyntempoConnectorConstants.ATTR_USER_DEFAULTPLANT).toString().isEmpty()) {
						userToUpdate.setDefaultPlant(userInRequest.getDefaultPlant());
					}

					if (parameters.get(SyntempoConnectorConstants.ATTR_USER_ENABLED) != null && !parameters.get(SyntempoConnectorConstants.ATTR_USER_ENABLED).toString().isEmpty()) {
						userToUpdate.setEnabled(userInRequest.getEnabled());
					}
					if (parameters.get(SyntempoConnectorConstants.ATTR_USER_ISLOCALACCOUNT) != null && !parameters.get(SyntempoConnectorConstants.ATTR_USER_ISLOCALACCOUNT).toString().isEmpty()) {
						userToUpdate.setIsLocalAccount(userInRequest.getIsLocalAccount());
					}
					if (parameters.get(SyntempoConnectorConstants.ATTR_USER_ISADMIN) != null && !parameters.get(SyntempoConnectorConstants.ATTR_USER_ISADMIN).toString().isEmpty()) {
						userToUpdate.setIsAdmin(userInRequest.getIsAdmin());
					}
					if (parameters.get(SyntempoConnectorConstants.ATTR_USER_ISUSER) != null && !parameters.get(SyntempoConnectorConstants.ATTR_USER_ISUSER).toString().isEmpty()) {
						userToUpdate.setIsUser(userInRequest.getIsUser());
					}
					if (parameters.get(SyntempoConnectorConstants.ATTR_USER_ISPLANTADMIN) != null && !parameters.get(SyntempoConnectorConstants.ATTR_USER_ISPLANTADMIN).toString().isEmpty()) {
						userToUpdate.setIsPlantAdministrator(userInRequest.getIsPlantAdministrator());
					}
					if (parameters.get(SyntempoConnectorConstants.ATTR_USER_ISREPORTADMIN) != null && !parameters.get(SyntempoConnectorConstants.ATTR_USER_ISREPORTADMIN).toString().isEmpty()) {
						userToUpdate.setIsReportAdministrator(userInRequest.getIsReportAdministrator());
					}
					if (parameters.get(SyntempoConnectorConstants.ATTR_USER_ISCONFIGURATOR) != null && !parameters.get(SyntempoConnectorConstants.ATTR_USER_ISCONFIGURATOR).toString().isEmpty()) {
						userToUpdate.setIsConfigurator(userInRequest.getIsConfigurator());
					}
					if (parameters.get(SyntempoConnectorConstants.ATTR_USER_ISALERTADMIN) != null && !parameters.get(SyntempoConnectorConstants.ATTR_USER_ISALERTADMIN).toString().isEmpty()) {
						userToUpdate.setIsAlertAdministrator(userInRequest.getIsAlertAdministrator());
					}
					if (parameters.get(SyntempoConnectorConstants.ATTR_USER_ISDOCADMIN) != null && !parameters.get(SyntempoConnectorConstants.ATTR_USER_ISDOCADMIN).toString().isEmpty()) {
						userToUpdate.setIsDocumentAdministrator(userInRequest.getIsDocumentAdministrator());
					}
					String endPoint = _baseUrl.concat(SyntempoConnectorConstants.ENDPOINT_CREATE_UPDATE_USER);
					SyntempoURLConnection rs2Connection = SyntempoClientHelper.getConnection(endPoint, SyntempoConnectorConstants.REQ_METHOD_TYPE_POST, _apiUserName, _apiPassword, _jwtKey, null, _issuer, _audience);
					Gson gson = new Gson();
					String gsonString = gson.toJson(userToUpdate);
					// logger.debug(CLASS_NAME + " update() request payload ::: " + gsonString);
					outputStream = rs2Connection.getOutputStream();
					outputStream.write(gsonString.getBytes());
					outputStream.flush();
					int serverResponse = rs2Connection.getResponseCode();
					logger.debug(CLASS_NAME + " http response code,  " + serverResponse);
					if (serverResponse == HttpStatus.SC_OK) {
						provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CHANGE_USER, IProvisoiningConstants.CHANGE_USER_SUCCESS, false, null);
					} else {
						provisioningResult.setMsgDesc(rs2Connection.getResponseMessage());
					}
				} else {
					logger.debug(CLASS_NAME + " update() user does not exist / already updated in the system");
					provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CHANGE_USER, IProvisoiningConstants.CHANGE_USER_FAILURE, true, "User does not exist in external system");

				}
			}
		} catch (Exception e) {
			logger.error(CLASS_NAME + " update()", e);
			provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CHANGE_USER, IProvisoiningConstants.CHANGE_USER_FAILURE, true, "Error during update the user");
		}
		logger.info(CLASS_NAME + " update(): End of update method");
		return provisioningResult;
	}

	/**
	 * 
	 * 
	 * This method is to delete the user account in external system.
	 * 
	 * soori - 2:16:34 pm
	 * 
	 * @param requestNumber
	 * @param roles
	 * @param parameters
	 * @param requestDetails
	 * @param attMapping
	 * @return
	 * @throws Exception
	 * @see com.alnt.access.provisioning.services.IConnectionInterface#deleteAccount(java.lang.Long, java.util.List, java.util.Map, java.util.List, java.util.Map)
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public IProvisioningResult deleteAccount(Long requestNumber, List roles, Map parameters, List requestDetails, Map<String, String> attMapping) throws Exception {
		logger.debug(CLASS_NAME + " deleteAccount(): Start of update method");
		IProvisioningResult provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_DELETE_USER, IProvisoiningConstants.DELETE_USER_FAILURE, true, "Error while deleting User in external system");
		ISystemInformation systemInformation = null;
		try {
			ConnectorUtil.logInputParamters(parameters, excludeLogAttrList, logger);
			if (parameters.get(externalUserIdAttribute) == null) {
				logger.debug(CLASS_NAME + " deleteAccount() userId does not passed!!");
				provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_DELETE_USER, IProvisoiningConstants.DELETE_USER_FAILURE, true, "userId does not passed!!");
			} else {
				String userIDtoDelete = parameters.get(externalUserIdAttribute).toString();
				systemInformation = isUserProvisioned(userIDtoDelete);
				String queryString = SyntempoConnectorConstants.QUERY_STRING_USERNAME.concat(userIDtoDelete);
				if (systemInformation != null && systemInformation.isProvisioned()) {
					String endPoint = _baseUrl.concat(SyntempoConnectorConstants.ENDPOINT_DELETE_USER);
					SyntempoURLConnection rs2Connection = SyntempoClientHelper.getConnection(endPoint, SyntempoConnectorConstants.REQ_METHOD_TYPE_DELETE, _apiUserName, _apiPassword, _jwtKey, queryString, _issuer, _audience);
					int serverResponse = rs2Connection.getResponseCode();
					logger.debug(CLASS_NAME + " http response code,  " + serverResponse);
					if (serverResponse == HttpStatus.SC_OK) {
						logger.debug(CLASS_NAME + " deleteAccount() user deleted successfully in the system");
						provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_DELETE_USER, IProvisoiningConstants.DELETE_USER_SUCCESS, false, null);
					} else {
						provisioningResult.setMsgDesc(rs2Connection.getResponseMessage());
					}
				} else {
					if (!_showProvisioningWarnings) {
						logger.debug(CLASS_NAME + " deleteAccount() user does not exist in the system");
						provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_DELETE_USER, IProvisoiningConstants.DELETE_USER_FAILURE, true, "User does not exist in external system");
					} else {
						logger.debug(CLASS_NAME + " deleteAccount() user does not exist , but warnings are disabled , returning as deleted successfully in the system");
						provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_DELETE_USER, IProvisoiningConstants.DELETE_USER_SUCCESS, false, null);
					}
				}
			}
		} catch (Exception e) {
			logger.error(CLASS_NAME + " deleteAccount(): Error during update the user ", e);
			logger.error(CLASS_NAME + " deleteAccount()", e);
		}
		return provisioningResult;
	}

	/**
	 * 
	 * soori 2019-12-06 - 11:54:34 am
	 * 
	 * @param requestNumber
	 * @param roles
	 * @param parameters
	 * @param requestDetails
	 * @param attMapping
	 * @return
	 * @throws Exception
	 * @see com.alnt.access.provisioning.services.IConnectionInterface#lock(java.lang.Long, java.util.List, java.util.Map, java.util.List, java.util.Map)
	 *
	 *
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public IProvisioningResult lock(Long requestNumber, List roles, Map parameters, List requestDetails, Map<String, String> attMapping) throws Exception {

		logger.info(CLASS_NAME + " lock(): Start of lock method");
		IProvisioningResult provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_LOCK_USER, IProvisoiningConstants.LOCK_USER_FAILURE, true, "lock user failed in target system!");
		OutputStream outputStream = null;
		try {
			if (parameters.get(externalUserIdAttribute) == null) {
				logger.debug(CLASS_NAME + " lock() userId does not passed!!");
				provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_LOCK_USER, IProvisoiningConstants.LOCK_USER_FAILURE, true, "userId does not passed!!");
			} else {
				String userIdToLock = parameters.get(externalUserIdAttribute).toString();
				UserDetails cardHolder = getUserByUserDetailsId(userIdToLock);
				if (null != cardHolder) {
					if (cardHolder.getEnabled()) {
						String endPoint = _baseUrl.concat(SyntempoConnectorConstants.ENDPOINT_CREATE_UPDATE_USER);
						SyntempoURLConnection rs2Connection = SyntempoClientHelper.getConnection(endPoint, SyntempoConnectorConstants.REQ_METHOD_TYPE_POST, _apiUserName, _apiPassword, _jwtKey, null, _issuer, _audience);
						Gson gson = new Gson();
						cardHolder.setEnabled(false);
						String gsonString = gson.toJson(cardHolder);
						// logger.debug(CLASS_NAME + " lock() request payload ::: " + gsonString);
						outputStream = rs2Connection.getOutputStream();
						outputStream.write(gsonString.getBytes());
						outputStream.flush();
						int serverResponse = rs2Connection.getResponseCode();
						logger.debug(CLASS_NAME + " http response code,  " + serverResponse);
						if (serverResponse == HttpStatus.SC_OK) {
							provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_LOCK_USER, IProvisoiningConstants.LOCK_USER_SUCCESS, false, null);
						} else {
							provisioningResult.setMsgDesc(rs2Connection.getResponseMessage());
						}
					} else {
						if (!_showProvisioningWarnings) {
							logger.debug(CLASS_NAME + "User already locked in the system");
							provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_LOCK_USER, IProvisoiningConstants.LOCK_USER_FAILURE, true, "User already locked in the system");
						} else {
							logger.debug(CLASS_NAME + "User already locked in the system");
							provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_LOCK_USER, IProvisoiningConstants.LOCK_USER_SUCCESS, true, "User already locked in the system");
						}
					}
				} else {
					logger.debug(CLASS_NAME + "lock() user does not exist");
					provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_LOCK_USER, IProvisoiningConstants.LOCK_USER_FAILURE, false, null);
				}
			}
		} catch (Exception e) {
			logger.error(CLASS_NAME + " lock()", e);
			provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_LOCK_USER, IProvisoiningConstants.LOCK_USER_FAILURE, true, "Error during lock the user");
		}
		logger.info(CLASS_NAME + " lock(): End of lock method");
		return provisioningResult;

	}

	/**
	 * 
	 * soori - 4:43:19 pm if request has card holder status as 2(date based , ie, card status in not NULL and should be integer value) request should pass valid from and valid to
	 * 
	 * 
	 * @param requestNumber
	 * @param roles
	 * @param parameters
	 * @param requestDetails
	 * @param attMapping
	 * @return
	 * @throws Exception
	 * @see com.alnt.access.provisioning.services.IConnectionInterface#unlock(java.lang.Long, java.util.List, java.util.Map, java.util.List, java.util.Map)
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public IProvisioningResult unlock(Long requestNumber, List roles, Map parameters, List requestDetails, Map<String, String> attMapping) throws Exception {
		logger.info(CLASS_NAME + " unlock(): Start of unlock method");
		IProvisioningResult provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_UNLOCK_USER, IProvisoiningConstants.UNLOCK_USER_FAILURE, true, "unlock user failed in target system!");
		OutputStream outputStream = null;
		try {
			ConnectorUtil.logInputParamters(parameters, excludeLogAttrList, logger);
			if (parameters.get(externalUserIdAttribute) == null) {
				logger.debug(CLASS_NAME + " unlock() userId does not passed!!");
				provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_UNLOCK_USER, IProvisoiningConstants.UNLOCK_USER_FAILURE, true, "userId does not passed!!");
			} else {
				String userIdToUnlock = parameters.get(externalUserIdAttribute).toString();
				UserDetails cardHolder = getUserByUserDetailsId(userIdToUnlock);
				if (null != cardHolder) {
					if (!cardHolder.getEnabled()) {
						String endPoint = _baseUrl.concat(SyntempoConnectorConstants.ENDPOINT_CREATE_UPDATE_USER);
						SyntempoURLConnection rs2Connection = SyntempoClientHelper.getConnection(endPoint, SyntempoConnectorConstants.REQ_METHOD_TYPE_POST, _apiUserName, _apiPassword, _jwtKey, null, _issuer, _audience);
						Gson gson = new Gson();
						cardHolder.setEnabled(true);
						String gsonString = gson.toJson(cardHolder);
						// logger.debug(CLASS_NAME + " unlock() request payload ::: " + gsonString);
						outputStream = rs2Connection.getOutputStream();
						outputStream.write(gsonString.getBytes());
						outputStream.flush();
						int serverResponse = rs2Connection.getResponseCode();
						logger.debug(CLASS_NAME + " http response code,  " + serverResponse);
						if (serverResponse == HttpStatus.SC_OK) {
							provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_UNLOCK_USER, IProvisoiningConstants.UNLOCK_USER_SUCCESS, false, null);
						} else {
							provisioningResult.setMsgDesc(rs2Connection.getResponseMessage());
						}
					} else {
						if (!_showProvisioningWarnings) {
							logger.debug(CLASS_NAME + " unlock() user already unlocked in the system");
							provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_UNLOCK_USER, IProvisoiningConstants.UNLOCK_USER_FAILURE, true, "User already active in the system");
						} else {
							logger.debug(CLASS_NAME + " unlock() user already unlocked in the system");
							provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_UNLOCK_USER, IProvisoiningConstants.UNLOCK_USER_SUCCESS, true, "User already active in the system");
						}
					}
				} else {
					logger.debug(CLASS_NAME + " unlock() user does not exist");
					provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_UNLOCK_USER, IProvisoiningConstants.UNLOCK_USER_FAILURE, false, "User does not exist in external system");
				}
			}
		} catch (Exception e) {
			logger.error(CLASS_NAME + " unlock()", e);
			provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_UNLOCK_USER, IProvisoiningConstants.UNLOCK_USER_FAILURE, true, "Error during unlock the user");
		}
		logger.info(CLASS_NAME + " unlock(): End of lock method");
		return provisioningResult;
	}

	/**
	 * 
	 * it required to pass alert date format in connection parameters.it should have time component the value should be same format , if not need to change parameter value in systems
	 * 
	 * 
	 * soori - 5:17:13 pm
	 * 
	 * @param requestNumber
	 * @param roles
	 * @param parameters
	 * @param requestDetails
	 * @param action
	 * @param attMapping
	 * @return
	 * @throws Exception
	 * @see com.alnt.access.provisioning.services.IConnectionInterface#delimitUser(java.lang.Long, java.util.List, java.util.Map, java.util.List, java.lang.String, java.util.Map)
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public IProvisioningResult delimitUser(Long requestNumber, List roles, Map parameters, List requestDetails, String action, Map<String, String> attMapping) throws Exception {
		return null;
	}

	/**
	 * 
	 * soori 2019-12-06 - 12:20:15 am
	 * 
	 * @param requestNumber
	 * @param roles
	 * @param parameters
	 * @param requestDetails
	 * @param attMapping
	 * @return
	 * @throws Exception
	 * @see com.alnt.access.provisioning.services.IConnectionInterface#activateBadge(java.lang.Long, java.util.List, java.util.Map, java.util.List, java.util.Map)
	 *
	 *
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public IProvisioningResult activateBadge(Long requestNumber, List roles, Map parameters, List requestDetails, Map<String, String> attMapping) throws Exception {
		return null;
	}

	/**
	 * 
	 * soori 2019-12-06 - 12:42:46 am
	 * 
	 * @param requestNumber
	 * @param roles
	 * @param parameters
	 * @param requestDetails
	 * @param attMapping
	 * @return
	 * @throws Exception
	 * @see com.alnt.access.provisioning.services.IConnectionInterface#deActivateBadge(java.lang.Long, java.util.List, java.util.Map, java.util.List, java.util.Map)
	 *
	 *
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public IProvisioningResult deActivateBadge(Long requestNumber, List roles, Map parameters, List requestDetails, Map<String, String> attMapping) throws Exception {
		return null;
	}

	/**
	 * 
	 * soori 2019-12-03 - 8:22:08 pm
	 * 
	 * @param requestNumber
	 * @param roles
	 * @param parameters
	 * @param requestDetails
	 * @param attMapping
	 * @return
	 * @throws Exception
	 * @see com.alnt.access.provisioning.services.IConnectionInterface#addBadge(java.lang.Long, java.util.List, java.util.Map, java.util.List, java.util.Map)
	 *
	 *
	 */
	@SuppressWarnings({ "rawtypes" })
	@Override
	public IProvisioningResult addBadge(Long requestNumber, List roles, Map parameters, List requestDetails, Map<String, String> attMapping) throws Exception {
		return null;
	}

	/**
	 * 
	 * soori 2019-12-04 - 4:44:43 pm
	 * 
	 * @param requestNumber
	 * @param roles
	 * @param parameters
	 * @param requestDetails
	 * @param attMapping
	 * @return
	 * @throws Exception
	 * @see com.alnt.access.provisioning.services.IConnectionInterface#addTempBadge(java.lang.Long, java.util.List, java.util.Map, java.util.List, java.util.Map)
	 *
	 *
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public IProvisioningResult addTempBadge(Long requestNumber, List roles, Map parameters, List requestDetails, Map<String, String> attMapping) throws Exception {
		return null;
	}

	/**
	 * 
	 * soori 2019-12-02 - 5:38:47 pm
	 * 
	 * @param requestNumber
	 * @param roles
	 * @param parameters
	 * @param requestDetails
	 * @param deprovisionRole
	 * @param attMappin
	 * @return
	 * @throws Exception
	 * @see com.alnt.access.provisioning.services.IConnectionInterface#changeAccess(java.lang.Long, java.util.List, java.util.Map, java.util.List, java.util.List, java.util.Map)
	 *
	 *
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public IProvisioningResult changeAccess(Long requestNumber, List provisionRoles, Map parameters, List requestDetails, List<IRoleInformation> deprovisionRoles, Map<String, String> attMapping) throws Exception {
		logger.debug(CLASS_NAME + " changeAccess(): Start of changeAccess method");
		IProvisioningResult provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CHANGE_ROLES, IProvisoiningConstants.ASSIGN_ROLES_FAILURE, true, "Error assigning roles");
		OutputStream outputStream = null;
		List<IRoleAuditInfo> roleAuditInfoList = new ArrayList<IRoleAuditInfo>();
		try {
			ConnectorUtil.logInputParamters(parameters, excludeLogAttrList, logger);
			if ((deprovisionRoles != null && !deprovisionRoles.isEmpty()) || (provisionRoles != null && !provisionRoles.isEmpty())) {
				if (parameters.get(externalUserIdAttribute) == null) {
					logger.debug(CLASS_NAME + " changeAccess() userId does not passed!!");
					provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CHANGE_ROLES, IProvisoiningConstants.ASSIGN_ROLES_FAILURE, true, "UserId does not passed!!");
				} else {
					String userIdToChangeAccess = parameters.get(externalUserIdAttribute).toString();
					UserDetails cardHolder = getUserByUserDetailsId(userIdToChangeAccess);
					if (null != cardHolder) {
						String[] rolesInSystem = cardHolder.getUserGroups();
						logger.trace(CLASS_NAME + " changeAccess() Existing Access levels: " + rolesInSystem != null ? rolesInSystem.length : "null");
						if (deprovisionRoles != null && !deprovisionRoles.isEmpty()) {
							Iterator<IRoleInformation> removeRoleItr = deprovisionRoles.iterator();
							List<String> list = new ArrayList<String>(Arrays.asList(rolesInSystem));
							while (removeRoleItr.hasNext()) {
								IRoleInformation roleInformation = (IRoleInformation) removeRoleItr.next();
								logger.debug(CLASS_NAME + " changeAccess() role requested to be removed  :: " + roleInformation.getName());
								if (list.contains(roleInformation.getName())) {
									list.remove(roleInformation.getName());
									rolesInSystem = list.toArray(new String[0]);
								}
								roleAuditInfoList.add(getRoleAuditInfo(roleInformation, IProvisoiningConstants.REMOVE_ROLE));
							}
						}
						if (provisionRoles != null && !provisionRoles.isEmpty()) {
							Iterator<IRoleInformation> addRoleItr = provisionRoles.iterator();
							while (addRoleItr.hasNext()) {
								ArrayList<String> rolesInSystemList = new ArrayList<String>(Arrays.asList(rolesInSystem));
								IRoleInformation roleInformation = (IRoleInformation) addRoleItr.next();
								logger.debug(CLASS_NAME + " changeAccess() role requested to be added  :: " + roleInformation.getName());
								rolesInSystemList.add(roleInformation.getName());
								roleAuditInfoList.add(getRoleAuditInfo(roleInformation, IProvisoiningConstants.ADD_ROLE));
								rolesInSystem = rolesInSystemList.toArray(rolesInSystem);
							}
						}

						cardHolder.setUserGroups(rolesInSystem);
						String endPoint = _baseUrl.concat(SyntempoConnectorConstants.ENDPOINT_CREATE_UPDATE_USER);
						SyntempoURLConnection rs2Connection = SyntempoClientHelper.getConnection(endPoint, SyntempoConnectorConstants.REQ_METHOD_TYPE_POST, _apiUserName, _apiPassword, _jwtKey, null, _issuer, _audience);
						Gson gson = new Gson();
						String gsonString = gson.toJson(cardHolder);
						// logger.debug(CLASS_NAME + " changeAccess() request payload ::: " + gsonString);
						outputStream = rs2Connection.getOutputStream();
						outputStream.write(gsonString.getBytes());
						outputStream.flush();
						int serverResponse = rs2Connection.getResponseCode();
						logger.debug(CLASS_NAME + " http response code,  " + serverResponse);
						if (serverResponse == HttpStatus.SC_OK) {
							provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CHANGE_ROLES, IProvisoiningConstants.ASSIGN_ROLES_SUCCESS, false, "Access changed successfully.");
						} else {
							provisioningResult.setMsgDesc(rs2Connection.getResponseMessage());
						}
					} else {
						logger.debug(CLASS_NAME + " changeAccess() user does not exist ");
						provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CHANGE_ROLES, IProvisoiningConstants.ASSIGN_ROLES_FAILURE, true, "User does not exist in external system");
					}
				}
			} else {
				provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CHANGE_ROLES, IProvisoiningConstants.ASSIGN_ROLES_SUCCESS, false, "Access changed successfully.");
			}
		} catch (Exception e) {
			logger.error(CLASS_NAME + " changeAccess(): Error Assigning the roles = ", e);
			roleAuditInfoList = new ArrayList<IRoleAuditInfo>();
			// Code Modified by Adepu
			provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CHANGE_ROLES, IProvisoiningConstants.ASSIGN_ROLES_FAILURE, true, "Error assigning roles" + e.getMessage());
		}
		provisioningResult.setRoleList(roleAuditInfoList);
		return provisioningResult;
	}

	/**
	 * 
	 * soori - 2019-12-06 - 1:39:21 pm
	 * 
	 * @param requestNumber
	 * @param roles
	 * @param parameters
	 * @param requestDetails
	 * @param attMapping
	 * @return IProvisioningResult
	 * @throws Exception
	 * @see com.alnt.access.provisioning.services.IConnectionInterface#changeBadge(java.lang.Long, java.util.List, java.util.Map, java.util.List, java.util.Map)
	 *
	 *
	 */
	@SuppressWarnings({ "rawtypes" })
	@Override
	public IProvisioningResult changeBadge(Long requestNumber, List roles, Map parameters, List requestDetails, Map<String, String> attMapping) throws Exception {
		return null;

	}

	/**
	 * 
	 * soori - 2019-12-06 - 2:33:51 pm
	 * 
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @param arg3
	 * @param arg4
	 * @return
	 * @throws Exception
	 * @see com.alnt.access.provisioning.services.IConnectionInterface#changeBadgeRoles(java.lang.Long, java.util.List, java.util.Map, java.util.List, java.util.Map)
	 *
	 *
	 */
	@SuppressWarnings({ "rawtypes" })
	@Override
	public IProvisioningResult changeBadgeRoles(Long requestNumber, List roles, Map parameters, List requestDetails, Map<String, String> attMapping) throws Exception {
		return null;
	}

	/**
	 * 
	 * soori 2019-12-04 - 5:07:00 pm
	 * 
	 * @param requestNumber
	 * @param roles
	 * @param parameters
	 * @param requestDetails
	 * @param attMapping
	 * @return
	 * @throws Exception
	 * @see com.alnt.access.provisioning.services.IConnectionInterface#removeBadge(java.lang.Long, java.util.List, java.util.Map, java.util.List, java.util.Map)
	 *
	 *
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public IProvisioningResult removeBadge(Long requestNumber, List roles, Map parameters, List requestDetails, Map<String, String> attMapping) throws Exception {
		return null;
	}

	/**
	 * 
	 * soori 2019-12-06 - 12:01:43 pm
	 * 
	 * @param roleInformation
	 * @param action
	 * @return IRoleAuditInfo
	 *
	 */
	private IRoleAuditInfo getRoleAuditInfo(IRoleInformation roleInformation, String action) {
		IRoleAuditInfo roleAuditInfo = new RoleAuditInfo();
		roleAuditInfo.setRoleName(roleInformation.getName());
		roleAuditInfo.setValidFrom(roleInformation.getValidFrom());
		roleAuditInfo.setValidTo(roleInformation.getValidTo());
		roleAuditInfo.setAction(action);
		return roleAuditInfo;
	}

	/**
	 * 
	 * soori - 2019-12-06 - 12:42:16 pm
	 * 
	 * @param provisioningAction
	 * @param msgCode
	 * @param isFailed
	 * @param msgDesc
	 * @return IProvisioningResult
	 *
	 */
	private IProvisioningResult prepareProvisioningResult(String provisioningAction, String msgCode, Boolean isFailed, String msgDesc) {
		IProvisioningResult provisioningResult = new ProvisioningResult();
		provisioningResult.setMsgCode(msgCode);
		if (null != isFailed) {
			provisioningResult.setProvFailed(isFailed);
		}
		provisioningResult.setMsgDesc(msgDesc);
		provisioningResult.setProvAction(provisioningAction);
		return provisioningResult;
	}

	@Override
	public void setTaskId(Long arg0) {
	}

	@Override
	public IProvisioningResult updatePassword(Long requestNumber, String userId, String origPwd, String newPwd) throws Exception {
		logger.info(CLASS_NAME + " updatePassword(): Start of method");
		IProvisioningResult provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CHANGE_USER, IProvisoiningConstants.CHANGE_USER_FAILURE, true, "update password failed in target system!");
		OutputStream outputStream = null;
		try {
			if (newPwd == null || (null != newPwd && newPwd.isEmpty())) {
				logger.debug(CLASS_NAME + " updatePassword() new password does not passed or  it is empty !!");
				provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CHANGE_USER, IProvisoiningConstants.CHANGE_USER_FAILURE, true, "new password does not passed!!");
				return provisioningResult;
			}
			if (userId == null) {
				logger.debug(CLASS_NAME + " updatePassword() userId does not passed!!");
				provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CHANGE_USER, IProvisoiningConstants.CHANGE_USER_FAILURE, true, "userId does not passed!!");
			} else {
				UserDetails userToUpdate = getUserByUserDetailsId(userId);
				if (null != userToUpdate) {
					userToUpdate.setPassword(PasswordEncryptHelper.encrypt(_keytoEncryptUserPassword, newPwd));
					String endPoint = _baseUrl.concat(SyntempoConnectorConstants.ENDPOINT_CREATE_UPDATE_USER);
					SyntempoURLConnection rs2Connection = SyntempoClientHelper.getConnection(endPoint, SyntempoConnectorConstants.REQ_METHOD_TYPE_POST, _apiUserName, _apiPassword, _jwtKey, null, _issuer, _audience);
					Gson gson = new Gson();
					String gsonString = gson.toJson(userToUpdate);
					outputStream = rs2Connection.getOutputStream();
					outputStream.write(gsonString.getBytes());
					outputStream.flush();
					int serverResponse = rs2Connection.getResponseCode();
					logger.debug(CLASS_NAME + " http response code,  " + serverResponse);
					if (serverResponse == HttpStatus.SC_OK) {
						provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CHANGE_USER, IProvisoiningConstants.CHANGE_USER_SUCCESS, false, null);
					} else {
						provisioningResult.setMsgDesc(rs2Connection.getResponseMessage());
					}
				} else {
					logger.debug(CLASS_NAME + " updatePassword() user does not exist in the system");
					provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CHANGE_USER, IProvisoiningConstants.CHANGE_USER_FAILURE, true, "User does not exist in external system");
				}
			}
		} catch (Exception e) {
			logger.error(CLASS_NAME + " updatePassword()", e);
			provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CHANGE_USER, IProvisoiningConstants.CHANGE_USER_FAILURE, true, "Error during update password the user");
		}
		logger.info(CLASS_NAME + " updatePassword(): End of method");
		return provisioningResult;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List getBadgeLastLocation(String arg0) throws Exception {

		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List getDetailsAsList(String arg0, String arg1) throws Exception {

		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List getExistingBadges(String arg0) throws Exception {

		return null;
	}

	@Override
	public IProvisioningStatus getProvisioningStatus(Map<String, String> arg0) throws Exception {

		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List provision(Long arg0, String arg1, List arg2, List<com.alnt.fabric.component.rolemanagement.search.IRoleInformation> arg3, Map arg4, List arg5, List<com.alnt.fabric.component.rolemanagement.search.IRoleInformation> arg6,
			Map<String, String> arg7) throws Exception {
		return null;
	}

	@SuppressWarnings("rawtypes")
	public IProvisioningResult create(Long arg0, List arg1, Map arg2, List arg3, Map<String, String> arg4, Map<String, String> arg5) throws Exception {
		return null;
	}
	// PROVISIONING OPERATIONS :: BEGIN

}
