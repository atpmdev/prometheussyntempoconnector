package com.alnt.connector.provisioning.helper;

import java.net.HttpURLConnection;
import java.net.URL;
import java.time.Instant;
import java.util.Date;

import javax.net.ssl.HttpsURLConnection;

import org.apache.log4j.Logger;

import com.alnt.connector.constants.SyntempoConnectorConstants;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;

public class SyntempoClientHelper {

	final private static String CLASS_NAME = SyntempoClientHelper.class.getName();
	static Logger logger = Logger.getLogger(CLASS_NAME);

	public static final boolean isHttpsProtocal(String restUrl) {
		return !restUrl.isEmpty() && restUrl.startsWith("https://");
	}

	public static SyntempoURLConnection getConnection(final String restURL, final String methodType, final String userId, final String password, String jwtKey, String queryString, String issuer, String audience) {
		if (isHttpsProtocal(restURL)) {
			return getRESTHttpsServiceConnection(restURL.trim(), methodType, userId, password, jwtKey, queryString, issuer, audience);
		} else {
			return getRESTHttpServiceConnection(restURL.trim(), methodType, userId, password, jwtKey, queryString, issuer, audience);
		}
	}

	public static SyntempoURLConnection getRESTHttpServiceConnection(final String restURL, final String methodType, final String userId, final String password, String jwtKey, String queryString, String issuer, String audience) {
		final String METHOD_NAME = "getRESTServiceConnection()";
		logger.debug(CLASS_NAME + "getRESTHttpServiceConnection  :: " + "restUrl" + restURL + "method type: " + methodType);
		SyntempoHttpURLConnection restHttpURLConnection = new SyntempoHttpURLConnection();
		HttpURLConnection conn = null;
		URL url = null;
		try {
			logger.debug(CLASS_NAME + METHOD_NAME + "Building URL:" + restURL + " methodType:" + methodType);
			String urlwithJWT = restURL.concat(SyntempoConnectorConstants.QUERY_STRING_JWT).concat(generateJwt(userId, jwtKey, issuer, audience));
			if (null != queryString) {
				urlwithJWT = urlwithJWT.concat(queryString);
			}
			String escapeRestURL = urlwithJWT.replaceAll(" ", "%20");
			url = new URL(escapeRestURL);
			System.out.println(url);
			logger.debug(CLASS_NAME + METHOD_NAME + "Opening connection");
			conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod(methodType);
			conn.setRequestProperty(SyntempoConnectorConstants.ACCEPT_TYPE, SyntempoConnectorConstants.JSON_CONTENT);
			conn.setRequestProperty(SyntempoConnectorConstants.CONTENT_TYPE, SyntempoConnectorConstants.JSON_CONTENT);
			conn.setDoOutput(true);
			restHttpURLConnection.setHttpURLConnection(conn);
			logger.debug(CLASS_NAME + METHOD_NAME + "returning HttpURLConnection connection");
		} catch (Exception ex) {
			logger.error(CLASS_NAME + METHOD_NAME + "Exception while opening the port : " + ex);
		}
		logger.debug(CLASS_NAME + METHOD_NAME + "ENDS");
		return restHttpURLConnection;
	}

	public static SyntempoURLConnection getRESTHttpsServiceConnection(final String restURL, final String methodType, final String userId, final String password, String jwtKey, String queryString, String issuer, String audience) {
		final String METHOD_NAME = "getRESTServiceConnection()";
		logger.debug(CLASS_NAME + METHOD_NAME + "Inside : getRESTServiceConnection ,restUrl" + restURL + "method type: " + methodType);
		SyntempoHttpsURLConnection restHttpsURLConnection = new SyntempoHttpsURLConnection();
		HttpsURLConnection conn = null;
		URL url = null;
		try {
			logger.debug(CLASS_NAME + METHOD_NAME + "Building URL:" + restURL + " methodType:" + methodType);
			String urlwithJWT = restURL.concat(SyntempoConnectorConstants.QUERY_STRING_JWT).concat(generateJwt(userId, jwtKey, issuer, audience));
			if (null != queryString) {
				urlwithJWT = urlwithJWT.concat(queryString);
			}
			String escapeRestURL = urlwithJWT.replaceAll(" ", "%20");
			url = new URL(escapeRestURL);
			logger.debug(CLASS_NAME + METHOD_NAME + "Opening connection");
			conn = (HttpsURLConnection) url.openConnection();
			conn.setRequestMethod(methodType);
			conn.setRequestProperty(SyntempoConnectorConstants.ACCEPT_TYPE, SyntempoConnectorConstants.JSON_CONTENT);
			conn.setRequestProperty(SyntempoConnectorConstants.CONTENT_TYPE, SyntempoConnectorConstants.JSON_CONTENT);
			conn.setDoOutput(true);
			restHttpsURLConnection.setHttpsURLConnection(conn);
			logger.debug(CLASS_NAME + METHOD_NAME + "returning HttpURLConnection connection");
		} catch (Exception ex) {
			logger.error(CLASS_NAME + METHOD_NAME + "Exception while opening the port : " + ex);
		}
		logger.debug(CLASS_NAME + METHOD_NAME + "ENDS");
		return restHttpsURLConnection;
	}

	static String generateJwt(String userName, String jwtKey, String issuer, String audience) {
		final String METHOD_NAME = "generateJwt()";
		try {
			Algorithm algorithmHS = Algorithm.HMAC256(jwtKey);

			String token = JWT.create().withIssuer(issuer).withAudience(audience).withIssuedAt(Date.from(Instant.now())).withExpiresAt(Date.from(Instant.now().plusSeconds(SyntempoConnectorConstants.JWT_EXPIRE_SECONDS)))
					.withClaim(SyntempoConnectorConstants.JWT_PROXY_USER, userName).sign(algorithmHS);
			/*
			 * String token = Jwts.builder().setIssuer(issuer).setAudience(audience).claim(SyntempoConnectorConstants.JWT_PROXY_USER, userName).setIssuedAt(Date.from(Instant.now()))
			 * .setExpiration(Date.from(Instant.now().plusSeconds(SyntempoConnectorConstants.JWT_EXPIRE_SECONDS))).signWith(SignatureAlgorithm.HS256, TextCodec.BASE64.decode(jwtKey)).compact();
			 */
			return token;
		} catch (JWTCreationException e) {
			return "";
		} catch (Throwable e) {
			logger.debug(CLASS_NAME + METHOD_NAME + e.getLocalizedMessage());
			return "";
		}
	}

}
