package com.alnt.connector.provisioning.helper;
import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import com.google.common.hash.Hashing;

public class PasswordEncryptHelper {
	
	public static String encrypt(String key ,String password)
	{
	    try {
	        byte[] keyHash = Hashing.sha512().hashString(key, StandardCharsets.UTF_8).asBytes();
	        byte[] aesKey = new byte[24];
	        System.arraycopy(keyHash, 0, aesKey, 0, 24);

	        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");

	        final byte[] ivKey = new byte[cipher.getBlockSize()];
	        new SecureRandom().nextBytes(ivKey);
	        IvParameterSpec iv = new IvParameterSpec(ivKey);

	        SecretKeySpec skeySpec = new SecretKeySpec(aesKey, "AES");

	        cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);
	        byte[] encrypted = cipher.doFinal(password.getBytes(StandardCharsets.UTF_8));

	        byte[] combined = new byte[iv.getIV().length + encrypted.length];
	        System.arraycopy(iv.getIV(), 0, combined, 0, iv.getIV().length);
	        System.arraycopy(encrypted, 0, combined, iv.getIV().length, encrypted.length);

	        String ret = new String(Base64.getEncoder().encode(combined));
	        return ret;
	    } catch (Exception ex) {
	        ex.printStackTrace();
	    }
	    return null;
	}
}
