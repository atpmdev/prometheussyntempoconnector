package com.alnt.connector.exception;

public class SyntempoConnectorException extends Exception{

	private static final long serialVersionUID = 410782776743392490L;

	public SyntempoConnectorException(String message) {
		super(message);
	}
}
