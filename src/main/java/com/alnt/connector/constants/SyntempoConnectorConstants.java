package com.alnt.connector.constants;

public class SyntempoConnectorConstants {

	public static final String CONNECTOR_NAME = "SyntempoConnector";
	public static final String SYNTEMPO_USERID = "UserName";
	public static final String AE_USERID = "UserId";

	// CONNECTOR SPECIFIC
	public static final String API_USER_NAME = "userName";
	public static final String API_PASSWORD = "password";
	public static final String JWT_KEY = "jwtKey";
	public static final String TEST_USER = "testUser";
	public static final String BASE_URL = "baseURL";
	public static final String CONN_PARAM_JWT_ISSUER = "jwtIssuer";
	public static final String CONN_PARAM_JWT_AUDIENCE = "jwtAudience";
	public static final String KEY_TO_ENCRYPT_USERPASSWORD = "keytoEncryptUserPassword";
	public static final String ENABLE_PROVISION_WARNINGS = "enableProvisioningWarnings";
	public static final String SHOW_PROVISION_WARNINGS = "showProvisioningWarnings";
	public static final String SENSITIVE_ATTRIBUTES = "sensitiveAttributes";

	public static final String REQ_HEADER_AUTHORIZATION = "Authorization";
	public static final String AUTHORIZATION_TYPE = "Basic ";
	public static final String USERID_PWD_SEPERATOR = ":";
	public static final String CONTENT_TYPE = "Content-Type";
	public static final String ACCEPT_TYPE = "Accept";
	public static final String JSON_CONTENT = "application/json";
	
	public static final String QUERY_STRING_JWT = "?jwt=";
	public static final String QUERY_STRING_USERNAME = "&username=";
	public static final String JWT_ISSUER = "External";
	public static final String JWT_AUDIENCE = "Syntempo";
	public static final String JWT_PROXY_USER = "proxy_user";
	public static final Integer JWT_EXPIRE_SECONDS = 120;
	

	// GENERIC METADATA
	public static final String REQ_METHOD_TYPE_GET = "GET";
	public static final String REQ_METHOD_TYPE_POST = "POST";
	public static final String REQ_METHOD_TYPE_HEAD = "HEAD";
	public static final String REQ_METHOD_TYPE_OPTIONS = "OPTIONS";
	public static final String REQ_METHOD_TYPE_PUT = "PUT";
	public static final String REQ_METHOD_TYPE_DELETE = "DELETE";
	public static final String REQ_METHOD_TYPE_TRACE = "TRACE";

	public static final String ATTR_TYPE_STRING = "String";
	public static final String ATTR_TYPE_INT = "Int";
	public static final String ATTR_TYPE_BOOLEAN = "Boolean";
	public static final String ATTR_TYPE_LIST = "List";
	public static final String ATTR_TYPE_DATE = "Date";

	// API ENDPOINTS
	public static final String ENDPOINT_USERS = "api/externalAccount/user";
	public static final String ENDPOINT_CREATE_UPDATE_USER = "api/externalAccount/save";
	public static final String ENDPOINT_DELETE_USER = "api/externalAccount/deleteUser";
	public static final String ENDPOINT_ROLES = "api/externalAccount/groups";
	public static final String ENDPOINT_GET_USER = "api/externalAccount/user";
	
	// CONNECTOR ATTRIBUTES
	public static final String ATTR_USER_LN = "LastName";
	public static final String ATTR_USER_FN = "FirstName";
	public static final String ATTR_USER_USERNAME = "UserName";
	public static final String ATTR_USER_PASSWORD = "Password";
	public static final String ATTR_USER_EMAIL = "Email";
	public static final String ATTR_USER_PHONE = "PhoneNumber";
	public static final String ATTR_USER_DATEFORMAT = "DateFormat";
	public static final String ATTR_USER_TIMEFORMAT = "TimeFormat";
	public static final String ATTR_USER_DEFAULTPLANT = "DefaultPlant";
	public static final String ATTR_USER_ENABLED = "Enabled";
	public static final String ATTR_USER_ISLOCALACCOUNT = "IsLocalAccount";
	public static final String ATTR_USER_ISADMIN = "IsAdmin";
	public static final String ATTR_USER_ISUSER = "IsUser";
	public static final String ATTR_USER_ISPLANTADMIN = "IsPlantAdministrator";
	public static final String ATTR_USER_ISREPORTADMIN = "IsReportAdministrator";
	public static final String ATTR_USER_ISCONFIGURATOR = "IsConfigurator";
	public static final String ATTR_USER_ISALERTADMIN = "IsAlertAdministrator";
	public static final String ATTR_USER_ISDOCADMIN = "IsDocumentAdministrator";
	public static final String ATTR_USER_PLANTCODES = "PlantCodes";
	public static final String ATTR_USER_GROUPS = "UserGroups";
	
	public static final String ATTR_ROLE_ID = "RoleID";
	public static final String ALERT_ROLE_NAME = "RoleName";
}

