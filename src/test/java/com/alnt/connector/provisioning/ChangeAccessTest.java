package com.alnt.connector.provisioning;

import static org.junit.Assert.assertEquals;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;

import org.json.JSONException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.alnt.access.common.constants.IProvisoiningConstants;
import com.alnt.access.provisioning.model.IProvisioningResult;
import com.alnt.connector.constants.SyntempoConnectorConstants;
import com.alnt.connector.provisioning.model.RoleInformation;
import com.alnt.connector.provisioning.services.SyntempoConnectionInterface;
import com.alnt.extractionconnector.common.constants.IExtractionConstants;
import com.alnt.extractionconnector.user.model.ExtractorAttributes;
import com.alnt.fabric.component.rolemanagement.search.IRoleInformation;

/**
 * 
 * @author soori
 *
 */
public class ChangeAccessTest {
	private Map<String, String> connectionParams = null;
	

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * 
	 * soori 2019-11-27 - 4:30:49 pm void
	 * 
	 * @throws Exception
	 *
	 */

	@Before
	public void setUp() throws Exception {
		Properties p = new Properties();
		p.load(new FileReader("src/test/resources/testdata.properties"));
		connectionParams = new HashMap<String, String>();
		connectionParams.put("baseURL", p.getProperty("baseURL"));
		connectionParams.put("userName", p.getProperty("userName"));
		connectionParams.put("password", p.getProperty("password"));
		connectionParams.put("jwtKey", p.getProperty("jwtKey"));
		connectionParams.put(SyntempoConnectorConstants.KEY_TO_ENCRYPT_USERPASSWORD , SyntempoConnectorConstants.KEY_TO_ENCRYPT_USERPASSWORD);
	}

	@After
	public void tearDown() throws Exception {
	}

	/**
	 * if both provisioning roles & deprovisioning roles are null/ empty should it
	 * return success or failure.
	 */

	@Test
	public void changeAccessWithoutRolesToChange() throws Exception {
		SyntempoConnectionInterface connectionInterface = new SyntempoConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		IProvisioningResult response = connectionInterface.changeAccess(null, null, userParameters, null, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.ASSIGN_ROLES_SUCCESS);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Test
	public void changeAccessForWithOutUserParam() throws Exception {
		SyntempoConnectionInterface connectionInterface = new SyntempoConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		List provisionRoles = new ArrayList();
		provisionRoles.add(prepareRoleInformation("HQN"));
		IProvisioningResult response = connectionInterface.changeAccess(null, provisionRoles, userParameters, null,
				null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.ASSIGN_ROLES_FAILURE);
		assertEquals(response.getMsgDesc(), "UserId does not passed!!");
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Test
	public void changeAccessForWithNullUserParam() throws Exception {
		SyntempoConnectionInterface connectionInterface = new SyntempoConnectionInterface(connectionParams);
		Map<String, Object> userParameters =null;
		List provisionRoles = new ArrayList();
		provisionRoles.add(prepareRoleInformation("HQN"));
		IProvisioningResult response = connectionInterface.changeAccess(null, provisionRoles, userParameters, null,
				null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.ASSIGN_ROLES_FAILURE);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Test
	public void changeAccessForWithRandomUserId() throws Exception {
		connectionParams.put(SyntempoConnectorConstants.SHOW_PROVISION_WARNINGS, "False");
		SyntempoConnectionInterface connectionInterface = new SyntempoConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put(SyntempoConnectorConstants.SYNTEMPO_USERID, UUID.randomUUID().toString());
		List provisionRoles = new ArrayList();
		provisionRoles.add(prepareRoleInformation("HQN"));
		IProvisioningResult response = connectionInterface.changeAccess(null, provisionRoles, userParameters, null,
				null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.ASSIGN_ROLES_FAILURE);
		assertEquals(response.getMsgDesc(), "User does not exist in external system");
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Test
	public void changeAccessForWithRandomUserIdbutNoWarningsFlag() throws Exception {
		SyntempoConnectionInterface connectionInterface = new SyntempoConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put(SyntempoConnectorConstants.SYNTEMPO_USERID, UUID.randomUUID().toString());
		List provisionRoles = new ArrayList();
		provisionRoles.add(prepareRoleInformation("HQN"));
		IProvisioningResult response = connectionInterface.changeAccess(null, provisionRoles, userParameters, null,
				null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.ASSIGN_ROLES_SUCCESS);
	}
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Test
	public void changeAccessAddValidRole() throws Exception {
		SyntempoConnectionInterface connectionInterface = new SyntempoConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_LN, "ChangeAccessLN");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_FN, "ChangeAccessFN");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_USERNAME, "ChangeAccessUserName");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_PASSWORD, "Passw0rd");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_DEFAULTPLANT, "ANO");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_PLANTCODES, "ANO");
		List roles = new ArrayList();
		roles.add(prepareRoleInformation("HQN"));
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_EMAIL, "asdca@dss.cc");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_ISUSER, true);
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_ENABLED, true);
		IProvisioningResult response = connectionInterface.create(12321L, roles, userParameters, null, null);
		List<IRoleInformation> rolesFromSys = convertAccessLevelsToRoleInfo(getAccessRolesFromSystem(), null, null);
		response = connectionInterface.changeAccess(null, rolesFromSys, userParameters, null, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.ASSIGN_ROLES_SUCCESS);
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Test
	public void changeAccessAddDeleteValidRole() throws Exception {
		SyntempoConnectionInterface connectionInterface = new SyntempoConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_LN, "ChangeAccessLN");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_FN, "ChangeAccessFN");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_USERNAME, "ChangeAccessUserName");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_PASSWORD, "Passw0rd");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_DEFAULTPLANT, "ANO");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_PLANTCODES, "ANO");
		List roles = new ArrayList();
		roles.add(prepareRoleInformation("HQN"));
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_EMAIL, "asdca@dss.cc");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_ISUSER, true);
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_ENABLED, true);
		IProvisioningResult response = connectionInterface.create(12321L, roles, userParameters, null, null);
		List<IRoleInformation> rolesFromSys = convertAccessLevelsToRoleInfo(getAccessRolesFromSystem(), null, null);
		response = connectionInterface.changeAccess(null, rolesFromSys, userParameters, null, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.ASSIGN_ROLES_SUCCESS);
		response = connectionInterface.changeAccess(null, null, userParameters, null, rolesFromSys, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.ASSIGN_ROLES_SUCCESS);
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
	}


	private IRoleInformation prepareRoleInformation(String name) {

		IRoleInformation roleInformation = new RoleInformation();
		roleInformation.setRoleType(IExtractionConstants.ROLE_TYPE_SINGLE);
		roleInformation.setName(name);
		roleInformation.setDescription(name);
		roleInformation.setValidFrom(null);
		roleInformation.setValidTo(null);
		return roleInformation;
	}

	private String[] getAccessRolesFromSystem() throws IOException, JSONException,Exception {
		return new String[] { "HQN", "ANO" };
	}

	private List<IRoleInformation> convertAccessLevelsToRoleInfo(String[] accessLevels, String startDate,
			String endDate) {
		List<IRoleInformation> rolesList = new ArrayList<IRoleInformation>();
		for (String accessLevel : accessLevels) {
			Map<String, List<ExtractorAttributes>> options = new HashMap<String, List<ExtractorAttributes>>();
			List<ExtractorAttributes> attributes = new ArrayList<ExtractorAttributes>();
			ExtractorAttributes roleAttribute = new ExtractorAttributes(SyntempoConnectorConstants.ALERT_ROLE_NAME, "",
					IExtractionConstants.TYPE.STRING);
			roleAttribute.setRoleAttr(true);
			attributes.add(roleAttribute);
			options.put(SyntempoConnectorConstants.ATTR_ROLE_ID, attributes);
			IRoleInformation roleInformation = getRoleInformation(options,accessLevel);
			Map<String, List<String>> memberData = new HashMap<String, List<String>>();
			getRoleAttribute(options, "", "", memberData);
			roleInformation.setMemberData(memberData);
			rolesList.add(roleInformation);
		}
		return rolesList;
	}

	private IRoleInformation getRoleInformation(Map<String, List<ExtractorAttributes>> options, String roleName) {
		IRoleInformation roleInformation = new RoleInformation();
		roleInformation.setRoleType(IExtractionConstants.ROLE_TYPE_SINGLE);
		roleInformation.setName(roleName);
		List<ExtractorAttributes> aeRoleNameAttr = options.get(SyntempoConnectorConstants.ATTR_ROLE_ID);
		if (aeRoleNameAttr != null) {
			for (ExtractorAttributes extractorAttributes : aeRoleNameAttr) {
				if (extractorAttributes.isRoleAttr() && SyntempoConnectorConstants.ALERT_ROLE_NAME.equals(extractorAttributes.getAttributeName())) {
					roleInformation.setName(roleName);
				}
			}
		}
		roleInformation.setDescription(roleName);
		roleInformation.setValidFrom(null);
		roleInformation.setValidTo(null);
	
		return roleInformation;
	}

	private void getRoleAttribute(Map<String, List<ExtractorAttributes>> options, String attributeName,
			String attributeValue, Map<String, List<String>> memberData) {
		if (options.containsKey(attributeName)) {
			List<String> values = new ArrayList<String>();
			List<ExtractorAttributes> companyAttr = options.get(attributeName);
			for (ExtractorAttributes extractorAttributes : companyAttr) {
				if (extractorAttributes.isRoleAttr()) {
					values = new ArrayList<String>();
					values.add("" + attributeValue);
					memberData.put(extractorAttributes.getAttributeName(), values);
				}
			}
		}
	}

}
