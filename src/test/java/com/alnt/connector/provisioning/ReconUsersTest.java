/**
 * soori
 * 2019-11-21 
 */
package com.alnt.connector.provisioning;

import static org.junit.Assert.assertTrue;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;

import org.apache.commons.lang.RandomStringUtils;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.alnt.connector.constants.SyntempoConnectorConstants;
import com.alnt.connector.provisioning.model.RoleInformation;
import com.alnt.connector.provisioning.services.SyntempoConnectionInterface;
import com.alnt.extractionconnector.common.constants.IExtractionConstants;
import com.alnt.extractionconnector.common.service.ISearchCallback;
import com.alnt.extractionconnector.user.model.ExtractorAttributes;
import com.alnt.fabric.component.rolemanagement.search.IRoleInformation;

/**
 * @author soori
 *
 */
public class ReconUsersTest {
	private Map<String, String> connectionParams = null;
	Map<String, List<ExtractorAttributes>> options = new HashMap<String, List<ExtractorAttributes>>();
	Properties p = new Properties();

	/**
	 * soori - 11:38:10 pm
	 * 
	 * void
	 * 
	 * @throws java.lang.Exception
	 *
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * soori - 11:38:10 pm
	 * 
	 * void
	 * 
	 * @throws java.lang.Exception
	 *
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * soori - 11:38:10 pm
	 * 
	 * void
	 * 
	 * @throws java.lang.Exception
	 *
	 */
	@Before
	public void setUp() throws Exception {
		Properties p = new Properties();
		p.load(new FileReader("src/test/resources/testdata.properties"));
		connectionParams = new HashMap<String, String>();
		connectionParams.put("baseURL", p.getProperty("baseURL"));
		connectionParams.put("userName", p.getProperty("userName"));
		connectionParams.put("password", p.getProperty("password"));
		connectionParams.put("jwtKey", p.getProperty("jwtKey"));
		connectionParams.put(SyntempoConnectorConstants.KEY_TO_ENCRYPT_USERPASSWORD , SyntempoConnectorConstants.KEY_TO_ENCRYPT_USERPASSWORD);
		
		List<ExtractorAttributes> attributes = null;
		ExtractorAttributes userAttribute = null;
		
		attributes = new ArrayList<ExtractorAttributes>();
		userAttribute = new ExtractorAttributes("MasterUserId", "",
				IExtractionConstants.TYPE.STRING);
		userAttribute.setUserAttr(true);
		attributes.add(userAttribute);
		userAttribute = new ExtractorAttributes("EmployeeId", "",
				IExtractionConstants.TYPE.STRING);
		userAttribute.setUserAttr(true);
		attributes.add(userAttribute);
		options.put(SyntempoConnectorConstants.ATTR_USER_USERNAME, attributes);
		attributes = new ArrayList<ExtractorAttributes>();
		userAttribute = new ExtractorAttributes(SyntempoConnectorConstants.ATTR_USER_FN, "",
				IExtractionConstants.TYPE.STRING);
		userAttribute.setUserAttr(true);
		attributes.add(userAttribute);
		options.put(SyntempoConnectorConstants.ATTR_USER_FN, attributes);
		attributes = new ArrayList<ExtractorAttributes>();
		userAttribute = new ExtractorAttributes(SyntempoConnectorConstants.ATTR_USER_LN, "",
				IExtractionConstants.TYPE.STRING);
		userAttribute.setUserAttr(true);
		attributes.add(userAttribute);
		options.put(SyntempoConnectorConstants.ATTR_USER_LN, attributes);
		
		attributes = new ArrayList<ExtractorAttributes>();
		userAttribute = new ExtractorAttributes(SyntempoConnectorConstants.ATTR_USER_DEFAULTPLANT, "",
				IExtractionConstants.TYPE.STRING);
		userAttribute.setUserAttr(true);
		attributes.add(userAttribute);
		options.put(SyntempoConnectorConstants.ATTR_USER_DEFAULTPLANT, attributes);
		
		attributes = new ArrayList<ExtractorAttributes>();
		userAttribute = new ExtractorAttributes(SyntempoConnectorConstants.ATTR_USER_PASSWORD, "",
				IExtractionConstants.TYPE.STRING);
		userAttribute.setUserAttr(true);
		attributes.add(userAttribute);
		options.put(SyntempoConnectorConstants.ATTR_USER_PASSWORD, attributes);
		
		
		attributes = new ArrayList<ExtractorAttributes>();
		userAttribute = new ExtractorAttributes(SyntempoConnectorConstants.ATTR_USER_EMAIL, "",
				IExtractionConstants.TYPE.STRING);
		userAttribute.setUserAttr(true);
		attributes.add(userAttribute);
		options.put(SyntempoConnectorConstants.ATTR_USER_EMAIL, attributes);
		
		attributes = new ArrayList<ExtractorAttributes>();
		userAttribute = new ExtractorAttributes(SyntempoConnectorConstants.ATTR_USER_PHONE, "",
				IExtractionConstants.TYPE.STRING);
		userAttribute.setUserAttr(true);
		attributes.add(userAttribute);
		options.put(SyntempoConnectorConstants.ATTR_USER_PHONE, attributes);

		attributes = new ArrayList<ExtractorAttributes>();
		userAttribute = new ExtractorAttributes(SyntempoConnectorConstants.ATTR_USER_ENABLED, "",
				IExtractionConstants.TYPE.STRING);
		userAttribute.setUserAttr(true);
		attributes.add(userAttribute);
		options.put(SyntempoConnectorConstants.ATTR_USER_ENABLED, attributes);
		
		
		attributes = new ArrayList<ExtractorAttributes>();
		userAttribute = new ExtractorAttributes(SyntempoConnectorConstants.ALERT_ROLE_NAME, "",
				IExtractionConstants.TYPE.STRING);
		userAttribute.setRoleAttr(true);
		attributes.add(userAttribute);
		userAttribute = new ExtractorAttributes("XYZ", "",
				IExtractionConstants.TYPE.STRING);
		userAttribute.setRoleAttr(true);
		attributes.add(userAttribute);
		
		userAttribute = new ExtractorAttributes("DisplayRoleName", "",
				IExtractionConstants.TYPE.STRING);
		userAttribute.setRoleAttr(true);
		attributes.add(userAttribute);
		options.put(SyntempoConnectorConstants.ATTR_ROLE_ID, attributes);
	}

	@After
	public void tearDown() throws Exception {
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Test
	public void createUserWithFullData() throws Exception {
		SyntempoConnectionInterface connectionInterface = new SyntempoConnectionInterface(connectionParams);
		for (int i = 0; i <= 100; i++) {
			Map<String, Object> userParameters = new HashMap<String, Object>();
			Random rd = new Random();
			userParameters.put(SyntempoConnectorConstants.ATTR_USER_LN, RandomStringUtils.randomAlphabetic(10));
			userParameters.put(SyntempoConnectorConstants.ATTR_USER_FN, RandomStringUtils.randomAlphabetic(10));
			userParameters.put(SyntempoConnectorConstants.ATTR_USER_USERNAME, RandomStringUtils.randomAlphabetic(30));
			userParameters.put(SyntempoConnectorConstants.ATTR_USER_PASSWORD, RandomStringUtils.randomAlphabetic(10));
			userParameters.put(SyntempoConnectorConstants.ATTR_USER_DEFAULTPLANT, "ANO");
			userParameters.put(SyntempoConnectorConstants.ATTR_USER_PLANTCODES, "ANO");
			List roles = new ArrayList();
			roles.add(prepareRoleInformation("HQN"));
			userParameters.put(SyntempoConnectorConstants.ATTR_USER_EMAIL, RandomStringUtils.randomAlphabetic(10) + "@" + RandomStringUtils.randomAlphabetic(4) + ".com");
			userParameters.put(SyntempoConnectorConstants.ATTR_USER_PHONE, RandomStringUtils.randomNumeric(10));
			userParameters.put(SyntempoConnectorConstants.ATTR_USER_DATEFORMAT, "yyyyMMdd");
			userParameters.put(SyntempoConnectorConstants.ATTR_USER_TIMEFORMAT, "HHmmss");
			userParameters.put(SyntempoConnectorConstants.ATTR_USER_ENABLED, rd.nextBoolean());
			userParameters.put(SyntempoConnectorConstants.ATTR_USER_ISLOCALACCOUNT, rd.nextBoolean());
			userParameters.put(SyntempoConnectorConstants.ATTR_USER_ISADMIN, rd.nextBoolean());
			userParameters.put(SyntempoConnectorConstants.ATTR_USER_ISPLANTADMIN, rd.nextBoolean());
			userParameters.put(SyntempoConnectorConstants.ATTR_USER_ISREPORTADMIN, rd.nextBoolean());
			userParameters.put(SyntempoConnectorConstants.ATTR_USER_ISCONFIGURATOR, rd.nextBoolean());
			userParameters.put(SyntempoConnectorConstants.ATTR_USER_ISALERTADMIN, rd.nextBoolean());
			userParameters.put(SyntempoConnectorConstants.ATTR_USER_ISDOCADMIN, rd.nextBoolean());
			userParameters.put(SyntempoConnectorConstants.ATTR_USER_ISUSER, true);
			connectionInterface.create(12321L, roles, userParameters, null, null);
		//	assertEquals(response.getMsgCode(), IProvisoiningConstants.CREATE_USER_SUCCESS);
			i++;
		}
		// connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
	}
	
	@Test
	public void userReconTest() throws Exception {
		SyntempoConnectionInterface connectionInterface = new SyntempoConnectionInterface(connectionParams);
		ISearchCallback callback = new SearchCallback();
		connectionInterface.getAllUsersWithCallback(options, 10, null, callback);
		assertTrue(true);
	}

	private IRoleInformation prepareRoleInformation(String name) {
		IRoleInformation roleInformation = new RoleInformation();
		roleInformation.setRoleType(IExtractionConstants.ROLE_TYPE_SINGLE);
		roleInformation.setName(name);
		roleInformation.setDescription(name);
		roleInformation.setValidFrom(null);
		roleInformation.setValidFrom(null);
		return roleInformation;
	}

}
