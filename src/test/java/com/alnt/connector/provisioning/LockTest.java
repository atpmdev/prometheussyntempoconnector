package com.alnt.connector.provisioning;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.alnt.access.common.constants.IProvisoiningConstants;
import com.alnt.access.provisioning.model.IProvisioningResult;
import com.alnt.connector.constants.SyntempoConnectorConstants;
import com.alnt.connector.provisioning.model.RoleInformation;
import com.alnt.connector.provisioning.services.SyntempoConnectionInterface;
import com.alnt.extractionconnector.common.constants.IExtractionConstants;
import com.alnt.fabric.component.rolemanagement.search.IRoleInformation;

/**
 * 
 * @author soori
 *
 */
public class LockTest {
	private Map<String, String> connectionParams = null;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * 
	 * soori 2019-11-27 - 4:30:49 pm void
	 * 
	 * @throws Exception
	 *
	 */

	@Before
	public void setUp() throws Exception {
		Properties p = new Properties();
		p.load(new FileReader("src/test/resources/testdata.properties"));
		connectionParams = new HashMap<String, String>();
		connectionParams.put("baseURL", p.getProperty("baseURL"));
		connectionParams.put("userName", p.getProperty("userName"));
		connectionParams.put("password", p.getProperty("password"));
		connectionParams.put("jwtKey", p.getProperty("jwtKey"));
		connectionParams.put(SyntempoConnectorConstants.KEY_TO_ENCRYPT_USERPASSWORD , SyntempoConnectorConstants.KEY_TO_ENCRYPT_USERPASSWORD);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void lockUserWithoutCardHolderIdParam() throws Exception {
		SyntempoConnectionInterface connectionInterface = new SyntempoConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		IProvisioningResult response = connectionInterface.lock(2321L, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.LOCK_USER_FAILURE);
	}
	
	@Test
	public void lockUserWitNullParam() throws Exception {
		SyntempoConnectionInterface connectionInterface = new SyntempoConnectionInterface(connectionParams);
		Map<String, Object> userParameters = null;
		IProvisioningResult response = connectionInterface.lock(2321L, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.LOCK_USER_FAILURE);
	}

	@Test
	public void lockUserRandomCardHolderIdParam() throws Exception {
		SyntempoConnectionInterface connectionInterface = new SyntempoConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put(SyntempoConnectorConstants.SYNTEMPO_USERID, UUID.randomUUID().toString());
		IProvisioningResult response = connectionInterface.lock(2321L, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.LOCK_USER_FAILURE);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Test
	public void lockUserUserProvisioned() throws Exception {
		SyntempoConnectionInterface connectionInterface = new SyntempoConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_LN, "LockLN");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_FN, "LockFN");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_USERNAME, "LockUserName");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_PASSWORD, "Passw0rd");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_DEFAULTPLANT, "ANO");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_PLANTCODES, "ANO");
		List roles = new ArrayList();
		roles.add(prepareRoleInformation("HQN"));
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_EMAIL, "asdca@dss.cc");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_ISUSER, true);
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_ENABLED, true);
		IProvisioningResult response = connectionInterface.create(12321L, roles, userParameters, null, null);
		response = connectionInterface.lock(2321L, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.LOCK_USER_SUCCESS);
		assertTrue(connectionInterface.isUserLocked(userParameters.get(SyntempoConnectorConstants.SYNTEMPO_USERID).toString()));
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
	}
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Test
	public void lockAlreadyDeletedUserConnParamShowWarningsTrue() throws Exception {
		connectionParams.put(SyntempoConnectorConstants.SHOW_PROVISION_WARNINGS, "False");
		SyntempoConnectionInterface connectionInterface = new SyntempoConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_LN, "LockLN");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_FN, "LockFN");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_USERNAME, "LockUserName");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_PASSWORD, "Passw0rd");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_DEFAULTPLANT, "ANO");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_PLANTCODES, "ANO");
		List roles = new ArrayList();
		roles.add(prepareRoleInformation("HQN"));
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_EMAIL, "asdca@dss.cc");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_ISUSER, true);
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_ENABLED, true);
		IProvisioningResult response = connectionInterface.create(12321L, roles, userParameters, null, null);
		response = connectionInterface.lock(2321L, null, userParameters, null, null);
		response = connectionInterface.lock(2321L, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.LOCK_USER_FAILURE);
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
	}
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Test
	public void lockAlreadyLockedUserConnParamShowWarningsTrue() throws Exception {
		connectionParams.put(SyntempoConnectorConstants.SHOW_PROVISION_WARNINGS, "False");
		SyntempoConnectionInterface connectionInterface = new SyntempoConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_LN, "LockLN");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_FN, "LockFN");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_USERNAME, "LockUserName");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_PASSWORD, "Passw0rd");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_DEFAULTPLANT, "ANO");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_PLANTCODES, "ANO");
		List roles = new ArrayList();
		roles.add(prepareRoleInformation("HQN"));
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_EMAIL, "asdca@dss.cc");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_ISUSER, true);
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_ENABLED, true);
		IProvisioningResult response = connectionInterface.create(12321L, roles, userParameters, null, null);
		response = connectionInterface.lock(2321L, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.LOCK_USER_FAILURE);
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
	}
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Test
	public void lockAlreadyDeletedUserwithoutConnParamDefaultTrue() throws Exception {
		SyntempoConnectionInterface connectionInterface = new SyntempoConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_LN, "LockLN");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_FN, "LockFN");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_USERNAME, "LockUserName");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_PASSWORD, "Passw0rd");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_DEFAULTPLANT, "ANO");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_PLANTCODES, "ANO");
		List roles = new ArrayList();
		roles.add(prepareRoleInformation("HQN"));
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_EMAIL, "asdca@dss.cc");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_ISUSER, true);
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_ENABLED, true);
		IProvisioningResult response = connectionInterface.create(12321L, roles, userParameters, null, null);
		response = connectionInterface.lock(2321L, null, userParameters, null, null);
		response = connectionInterface.lock(2321L, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.LOCK_USER_SUCCESS);
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
	}
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Test
	public void lockAlreadyDeletedUserConnParamShowWarningsFalse() throws Exception {
		SyntempoConnectionInterface connectionInterface = new SyntempoConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_LN, "LockLN");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_FN, "LockFN");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_USERNAME, "LockUserName");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_PASSWORD, "Passw0rd");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_DEFAULTPLANT, "ANO");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_PLANTCODES, "ANO");
		List roles = new ArrayList();
		roles.add(prepareRoleInformation("HQN"));
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_EMAIL, "asdca@dss.cc");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_ISUSER, true);
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_ENABLED, true);
		IProvisioningResult response = connectionInterface.create(12321L, roles, userParameters, null, null);
		response = connectionInterface.lock(2321L, null, userParameters, null, null);
		response = connectionInterface.lock(2321L, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.LOCK_USER_SUCCESS);
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
	}
	private IRoleInformation prepareRoleInformation(String name) {
		IRoleInformation roleInformation = new RoleInformation();
		roleInformation.setRoleType(IExtractionConstants.ROLE_TYPE_SINGLE);
		roleInformation.setName(name);
		roleInformation.setDescription(name);
		roleInformation.setValidFrom(null);
		roleInformation.setValidFrom(null);
		return roleInformation;
	}

}
