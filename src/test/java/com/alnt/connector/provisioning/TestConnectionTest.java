/**
 * 
 */
package com.alnt.connector.provisioning;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.alnt.connector.constants.SyntempoConnectorConstants;
import com.alnt.connector.exception.SyntempoConnectorException;
import com.alnt.connector.provisioning.services.SyntempoConnectionInterface;

/**
 * @author soori
 *
 */
public class TestConnectionTest {
	private Map<String, String> connectionParams = null;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		Properties p = new Properties();
		p.load(new FileReader("src/test/resources/testdata.properties"));
		connectionParams = new HashMap<String, String>();
		connectionParams.put("baseURL", p.getProperty("baseURL"));
		connectionParams.put("userName", p.getProperty("userName"));
		connectionParams.put("password", p.getProperty("password"));
		connectionParams.put("jwtKey", p.getProperty("jwtKey"));
		connectionParams.put(SyntempoConnectorConstants.KEY_TO_ENCRYPT_USERPASSWORD, SyntempoConnectorConstants.KEY_TO_ENCRYPT_USERPASSWORD);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	@Test(expected = SyntempoConnectorException.class)
	public void nullConnectionParams() throws Exception {
		new SyntempoConnectionInterface(null);
	}

	@Test
	public void validCredentials() throws Exception {
		SyntempoConnectionInterface connectionInterface = new SyntempoConnectionInterface(connectionParams);
		boolean testPassed = connectionInterface.testConnection();
		assertTrue(testPassed);
	}

	@Test
	public void invalidUser() throws Exception {
		connectionParams.put("userName", null);
		SyntempoConnectionInterface connectionInterface = new SyntempoConnectionInterface(connectionParams);
		boolean testConnectionResponse = connectionInterface.testConnection();
		assertFalse(testConnectionResponse);
	}

	@Test
	public void invalidTestUser() throws Exception {
		connectionParams.put("testUser", "InvalidUser");
		SyntempoConnectionInterface connectionInterface = new SyntempoConnectionInterface(connectionParams);
		boolean testConnectionResponse = connectionInterface.testConnection();
		assertFalse(testConnectionResponse);
	}

	@Test
	public void wrongEndpoint() throws Exception {
		connectionParams.put("baseURL", "ssdfsdf");
		SyntempoConnectionInterface connectionInterface = new SyntempoConnectionInterface(connectionParams);
		boolean testConnectionResponse = connectionInterface.testConnection();
		assertFalse(testConnectionResponse);
	}

	@Test
	public void emptyEndpoint() throws Exception {
		connectionParams.put("baseURL", "");
		SyntempoConnectionInterface connectionInterface = new SyntempoConnectionInterface(connectionParams);
		boolean testConnectionResponse = connectionInterface.testConnection();
		assertFalse(testConnectionResponse);
	}

}
