/**
 * soori
 * 2019-11-21 
 */
package com.alnt.connector.provisioning;

import static org.junit.Assert.assertTrue;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.alnt.connector.constants.SyntempoConnectorConstants;
import com.alnt.connector.provisioning.services.SyntempoConnectionInterface;
import com.alnt.extractionconnector.common.constants.IExtractionConstants;
import com.alnt.extractionconnector.common.service.ISearchCallback;
import com.alnt.extractionconnector.user.model.ExtractorAttributes;

/**
 * @author soori
 *
 */
public class ReconRolesTest {
	private Map<String, String> connectionParams = null;

	/**
	 * soori - 11:38:10 pm
	 * 
	 * void
	 * 
	 * @throws java.lang.Exception
	 *
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * soori - 11:38:10 pm
	 * 
	 * void
	 * 
	 * @throws java.lang.Exception
	 *
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * soori - 11:38:10 pm
	 * 
	 * void
	 * 
	 * @throws java.lang.Exception
	 *
	 */
	@Before
	public void setUp() throws Exception {
		Properties p = new Properties();
		p.load(new FileReader("src/test/resources/testdata.properties"));
		connectionParams = new HashMap<String, String>();
		connectionParams.put("baseURL", p.getProperty("baseURL"));
		connectionParams.put("userName", p.getProperty("userName"));
		connectionParams.put("password", p.getProperty("password"));
		connectionParams.put("jwtKey", p.getProperty("jwtKey"));
		connectionParams.put(SyntempoConnectorConstants.KEY_TO_ENCRYPT_USERPASSWORD , SyntempoConnectorConstants.KEY_TO_ENCRYPT_USERPASSWORD);
	}

	/**
	 * soori - 11:38:10 pm
	 * 
	 * void
	 * 
	 * @throws java.lang.Exception
	 *
	 */
	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void fullRoleReconTest() throws Exception {
		Map<String, List<ExtractorAttributes>> options = new HashMap<String, List<ExtractorAttributes>>();
		List<ExtractorAttributes> attributes = new ArrayList<ExtractorAttributes>();
		ExtractorAttributes roleAttribute = new ExtractorAttributes(SyntempoConnectorConstants.ATTR_ROLE_ID, "",
				IExtractionConstants.TYPE.STRING);
		roleAttribute.setRoleAttr(true);
		attributes.add(roleAttribute);
		options.put(SyntempoConnectorConstants.ATTR_ROLE_ID, attributes);
		attributes = new ArrayList<ExtractorAttributes>();
		roleAttribute = new ExtractorAttributes(SyntempoConnectorConstants.ALERT_ROLE_NAME, "",
				IExtractionConstants.TYPE.STRING);
		roleAttribute.setRoleAttr(true);
		attributes.add(roleAttribute);
		options.put(SyntempoConnectorConstants.ALERT_ROLE_NAME, attributes);
		SyntempoConnectionInterface connectionInterface = new SyntempoConnectionInterface(connectionParams);
		ISearchCallback callback  =  new SearchCallback();
		connectionInterface.getAllRoles(options, 0, null, callback);
		assertTrue(true);	
	}

}
