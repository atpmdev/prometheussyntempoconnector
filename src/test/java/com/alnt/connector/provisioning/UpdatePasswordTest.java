package com.alnt.connector.provisioning;

import static org.junit.Assert.assertEquals;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.alnt.access.common.constants.IProvisoiningConstants;
import com.alnt.access.provisioning.model.IProvisioningResult;
import com.alnt.connector.constants.SyntempoConnectorConstants;
import com.alnt.connector.provisioning.model.RoleInformation;
import com.alnt.connector.provisioning.services.SyntempoConnectionInterface;
import com.alnt.extractionconnector.common.constants.IExtractionConstants;
import com.alnt.fabric.component.rolemanagement.search.IRoleInformation;

/**
 * 
 * @author soori
 * 
 * this test case can't validate old password
 *
 */
public class UpdatePasswordTest {
	private Map<String, String> connectionParams = null;
	Properties p = new Properties();

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * 
	 * soori 2019-11-27 - 4:30:49 pm void
	 * 
	 * @throws Exception
	 *
	 */

	@Before
	public void setUp() throws Exception {
		Properties p = new Properties();
		p.load(new FileReader("src/test/resources/testdata.properties"));
		connectionParams = new HashMap<String, String>();
		connectionParams.put("baseURL", p.getProperty("baseURL"));
		connectionParams.put("userName", p.getProperty("userName"));
		connectionParams.put("password", p.getProperty("password"));
		connectionParams.put("jwtKey", p.getProperty("jwtKey"));
		connectionParams.put(SyntempoConnectorConstants.KEY_TO_ENCRYPT_USERPASSWORD , SyntempoConnectorConstants.KEY_TO_ENCRYPT_USERPASSWORD);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void updateEmptyPassword() throws Exception {
		SyntempoConnectionInterface connectionInterface = new SyntempoConnectionInterface(connectionParams);
		IProvisioningResult response = connectionInterface.updatePassword(123L, null, null, "");
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CHANGE_USER_FAILURE);
		assertEquals(response.getMsgDesc(), "new password does not passed!!");
	}
	
	@Test
	public void updateNullPassword() throws Exception {
		SyntempoConnectionInterface connectionInterface = new SyntempoConnectionInterface(connectionParams);
		IProvisioningResult response = connectionInterface.updatePassword(123L, null, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CHANGE_USER_FAILURE);
		assertEquals(response.getMsgDesc(), "new password does not passed!!");
	}
	
	@Test
	public void updateNullUserId() throws Exception {
		SyntempoConnectionInterface connectionInterface = new SyntempoConnectionInterface(connectionParams);
		IProvisioningResult response = connectionInterface.updatePassword(123L, null, null, "PassWord");
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CHANGE_USER_FAILURE);
		assertEquals(response.getMsgDesc(), "userId does not passed!!");
	}
	
	@Test
	public void updateInvaidUserId() throws Exception {
		SyntempoConnectionInterface connectionInterface = new SyntempoConnectionInterface(connectionParams);
		IProvisioningResult response = connectionInterface.updatePassword(123L, "InvalidUser", null, "PassW0rd");
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CHANGE_USER_FAILURE);
		assertEquals(response.getMsgDesc(), "User does not exist in external system");
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Test
	public void updatePasswordProvisionedUser() throws Exception {
		SyntempoConnectionInterface connectionInterface = new SyntempoConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		String usName = "UwdUerName";
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_LN, "UpdateLN");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_FN, "UpdateFN");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_USERNAME, usName);
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_PASSWORD, "Passw0rd");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_DEFAULTPLANT, "ANO");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_PLANTCODES, "ANO");
		List roles = new ArrayList();
		roles.add(prepareRoleInformation("HQN"));
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_EMAIL, "dsdad@sad.d");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_PHONE, "123123333");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_DATEFORMAT, "ddMMyyyy");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_TIMEFORMAT, "HHmmss");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_ENABLED, true);
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_ISLOCALACCOUNT, true);
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_ISADMIN, true);
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_ISPLANTADMIN, true);
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_ISREPORTADMIN, true);
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_ISCONFIGURATOR,true);
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_ISALERTADMIN, true);
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_ISDOCADMIN, true);
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_ISUSER, true);
		IProvisioningResult response = connectionInterface.create(12321L, roles, userParameters, null, null);
		response = connectionInterface.updatePassword(123L, usName, null, "NewPassWkrd");
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CHANGE_USER_SUCCESS);
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Test
	public void updateInvalidPasswordProvisionedUser() throws Exception {
		SyntempoConnectionInterface connectionInterface = new SyntempoConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		String usName = "UpdatePwdUName";
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_LN, "UpdateLN");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_FN, "UpdateFN");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_USERNAME, usName);
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_PASSWORD, "Password");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_DEFAULTPLANT, "ANO");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_PLANTCODES, "ANO");
		List roles = new ArrayList();
		roles.add(prepareRoleInformation("HQN"));
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_EMAIL, "dsdad@sad.d");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_PHONE, "123123333");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_DATEFORMAT, "ddMMyyyy");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_TIMEFORMAT, "HHmmss");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_ENABLED, true);
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_ISLOCALACCOUNT, true);
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_ISADMIN, true);
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_ISPLANTADMIN, true);
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_ISREPORTADMIN, true);
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_ISCONFIGURATOR,true);
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_ISALERTADMIN, true);
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_ISDOCADMIN, true);
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_ISUSER, true);
		IProvisioningResult response = connectionInterface.create(12321L, roles, userParameters, null, null);
		response = connectionInterface.updatePassword(123L, usName, null, "12"); //not met password length
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CHANGE_USER_FAILURE);
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
	}
		
	private IRoleInformation prepareRoleInformation(String name) {
		IRoleInformation roleInformation = new RoleInformation();
		roleInformation.setRoleType(IExtractionConstants.ROLE_TYPE_SINGLE);
		roleInformation.setName(name);
		roleInformation.setDescription(name);
		roleInformation.setValidFrom(null);
		roleInformation.setValidFrom(null);
		return roleInformation;
	}
}
