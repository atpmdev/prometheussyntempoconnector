/**
 * soori
 * 2019-11-21 
 */
package com.alnt.connector.provisioning;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.alnt.access.common.constants.IProvisoiningConstants;
import com.alnt.access.provisioning.model.IProvisioningResult;
import com.alnt.access.provisioning.model.ISystemInformation;
import com.alnt.connector.constants.SyntempoConnectorConstants;
import com.alnt.connector.provisioning.model.RoleInformation;
import com.alnt.connector.provisioning.services.SyntempoConnectionInterface;
import com.alnt.extractionconnector.common.constants.IExtractionConstants;
import com.alnt.fabric.component.rolemanagement.search.IRoleInformation;

/**
 * @author soori
 *
 */
public class IsUserProvisionedTest {
	private Map<String, String> connectionParams = null;
	Properties p = new Properties();
	String imageUrl = null;

	/**
	 * soori - 11:38:10 pm
	 * 
	 * void
	 * 
	 * @throws java.lang.Exception
	 *
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * soori - 11:38:10 pm
	 * 
	 * void
	 * 
	 * @throws java.lang.Exception
	 *
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * soori - 11:38:10 pm
	 * 
	 * void
	 * 
	 * @throws java.lang.Exception
	 *
	 */
	@Before
	public void setUp() throws Exception {
		Properties p = new Properties();
		p.load(new FileReader("src/test/resources/testdata.properties"));
		connectionParams = new HashMap<String, String>();
		connectionParams.put("baseURL", p.getProperty("baseURL"));
		connectionParams.put("userName", p.getProperty("userName"));
		connectionParams.put("password", p.getProperty("password"));
		connectionParams.put("jwtKey", p.getProperty("jwtKey"));
		connectionParams.put(SyntempoConnectorConstants.KEY_TO_ENCRYPT_USERPASSWORD , SyntempoConnectorConstants.KEY_TO_ENCRYPT_USERPASSWORD);
	}

	@After
	public void tearDown() throws Exception {
	}

	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Test
	public void isFailedUserProvisioned() throws Exception {
		SyntempoConnectionInterface connectionInterface = new SyntempoConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_LN, "IsUserProvisionedLN");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_FN, "IsUserProvisionedFN");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_USERNAME, "IsUserProvisionedUserName");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_PASSWORD, "Passw0rd");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_PLANTCODES, "ANO;PLP");
		List roles = new ArrayList();
		roles.add(prepareRoleInformation("HQN"));
		roles.add(prepareRoleInformation("PLP"));
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_EMAIL, "asdca@dss.cc");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_ISUSER, true);
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_ENABLED, true);
		connectionInterface.create(12321L, roles, userParameters, null, null);
		ISystemInformation systemInfo = connectionInterface.isUserProvisioned(userParameters.get(SyntempoConnectorConstants.ATTR_USER_USERNAME).toString());
		assertTrue(!systemInfo.isProvisioned());
	}
	
	@Test
	public void isNullUserProvisioned() throws Exception {
		SyntempoConnectionInterface connectionInterface = new SyntempoConnectionInterface(connectionParams);
		ISystemInformation systemInfo = connectionInterface.isUserProvisioned(null);
		assertTrue(!systemInfo.isProvisioned());
	}
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Test
	public void validateCreatedUserIsProvisioned() throws Exception {
		SyntempoConnectionInterface connectionInterface = new SyntempoConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_LN, "IsUserProvisionedLN");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_FN, "IsUserProvisionedFN");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_USERNAME, "IsUserProvisionedUserName");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_PASSWORD, "Passw0rd");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_DEFAULTPLANT, "ANO");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_PLANTCODES, "ANO");
		List roles = new ArrayList();
		roles.add(prepareRoleInformation("HQN"));
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_EMAIL, "asdca@dss.cc");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_ISUSER, true);
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_ENABLED, true);
		IProvisioningResult response = connectionInterface.create(12321L, roles, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CREATE_USER_SUCCESS);
		ISystemInformation systemInfo = connectionInterface.isUserProvisioned(userParameters.get(SyntempoConnectorConstants.ATTR_USER_USERNAME).toString());
		assertTrue(systemInfo.isProvisioned());
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
	}

	private IRoleInformation prepareRoleInformation(String name) {
		IRoleInformation roleInformation = new RoleInformation();
		roleInformation.setRoleType(IExtractionConstants.ROLE_TYPE_SINGLE);
		roleInformation.setName(name);
		roleInformation.setDescription(name);
		roleInformation.setValidFrom(null);
		roleInformation.setValidFrom(null);
		return roleInformation;
	}

}
