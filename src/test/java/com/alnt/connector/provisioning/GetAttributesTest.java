/**
 * soori
 * 2019-11-21 
 */
package com.alnt.connector.provisioning;

import static org.junit.Assert.assertFalse;

import java.io.FileReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.alnt.connector.constants.SyntempoConnectorConstants;
import com.alnt.connector.provisioning.services.SyntempoConnectionInterface;

/**
 * @author soori
 *
 */
public class GetAttributesTest {
	private Map<String, String> connectionParams = null;

	/**
	 * soori - 11:38:10 pm
	 * 
	 * void
	 * 
	 * @throws java.lang.Exception
	 *
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * soori - 11:38:10 pm
	 * 
	 * void
	 * 
	 * @throws java.lang.Exception
	 *
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * 
	 * @throws Exception
	 */
	@Before
	public void setUp() throws Exception {
		Properties p = new Properties();
		p.load(new FileReader("src/test/resources/testdata.properties"));
		connectionParams = new HashMap<String, String>();
		connectionParams.put("baseURL", p.getProperty("baseURL"));
		connectionParams.put("userName", p.getProperty("userName"));
		connectionParams.put("password", p.getProperty("password"));
		connectionParams.put("jwtKey", p.getProperty("jwtKey"));
		connectionParams.put(SyntempoConnectorConstants.KEY_TO_ENCRYPT_USERPASSWORD , SyntempoConnectorConstants.KEY_TO_ENCRYPT_USERPASSWORD);
	}

	/**
	 * 
	 * @throws Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	@SuppressWarnings("rawtypes")
	@Test
	public void getAttributesTest() throws Exception {
		SyntempoConnectionInterface connectionInterface = new SyntempoConnectionInterface(connectionParams);
		List response = connectionInterface.getAttributes();
		// size is  19
		assertFalse(response.isEmpty());
	}
}
