package com.alnt.connector.provisioning;

import static org.junit.Assert.assertEquals;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.alnt.access.common.constants.IProvisoiningConstants;
import com.alnt.access.provisioning.model.IProvisioningResult;
import com.alnt.connector.constants.SyntempoConnectorConstants;
import com.alnt.connector.provisioning.model.RoleInformation;
import com.alnt.connector.provisioning.services.SyntempoConnectionInterface;
import com.alnt.extractionconnector.common.constants.IExtractionConstants;
import com.alnt.fabric.component.rolemanagement.search.IRoleInformation;

/**
 * 
 * @author soori
 *
 */
public class UpdateTest {
	private Map<String, String> connectionParams = null;
	Properties p = new Properties();

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * 
	 * soori 2019-11-27 - 4:30:49 pm void
	 * 
	 * @throws Exception
	 *
	 */

	@Before
	public void setUp() throws Exception {
		Properties p = new Properties();
		p.load(new FileReader("src/test/resources/testdata.properties"));
		connectionParams = new HashMap<String, String>();
		connectionParams.put("baseURL", p.getProperty("baseURL"));
		connectionParams.put("userName", p.getProperty("userName"));
		connectionParams.put("password", p.getProperty("password"));
		connectionParams.put("jwtKey", p.getProperty("jwtKey"));
		connectionParams.put(SyntempoConnectorConstants.KEY_TO_ENCRYPT_USERPASSWORD, SyntempoConnectorConstants.KEY_TO_ENCRYPT_USERPASSWORD);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void updateUserWithoutCardHolderIdParam() throws Exception {
		SyntempoConnectionInterface connectionInterface = new SyntempoConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		IProvisioningResult response = connectionInterface.update(2321L, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CHANGE_USER_FAILURE);
	}

	@Test
	public void updateUserWitNullParam() throws Exception {
		SyntempoConnectionInterface connectionInterface = new SyntempoConnectionInterface(connectionParams);
		Map<String, Object> userParameters = null;
		IProvisioningResult response = connectionInterface.update(2321L, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CHANGE_USER_FAILURE);
	}

	@Test
	public void updateUserRandomCardHolderIdParam() throws Exception {
		connectionParams.put(SyntempoConnectorConstants.SHOW_PROVISION_WARNINGS, "False");
		SyntempoConnectionInterface connectionInterface = new SyntempoConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put(SyntempoConnectorConstants.SYNTEMPO_USERID, UUID.randomUUID().toString());
		IProvisioningResult response = connectionInterface.update(2321L, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CHANGE_USER_FAILURE);
	}

	@Test
	public void updateUserRandomCardHolderIdAndIgnoreErrorsParam() throws Exception {
		SyntempoConnectionInterface connectionInterface = new SyntempoConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put(SyntempoConnectorConstants.SYNTEMPO_USERID, UUID.randomUUID().toString());
		IProvisioningResult response = connectionInterface.update(2321L, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CHANGE_USER_SUCCESS);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Test
	public void updateUserUserProvisioned() throws Exception {
		SyntempoConnectionInterface connectionInterface = new SyntempoConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_LN, "UpdateLN");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_FN, "UpdateFN");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_USERNAME, "UpdateUserName");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_PASSWORD, "Passw0rd");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_DEFAULTPLANT, "ANO");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_PLANTCODES, "ANO");
		List roles = new ArrayList();
		roles.add(prepareRoleInformation("HQN"));
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_EMAIL, "asdca@dss.cc");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_ISUSER, true);
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_ENABLED, true);
		IProvisioningResult response = connectionInterface.create(12321L, roles, userParameters, null, null);
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_LN, "UpdateLN updated");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_FN, "UpdateFN updated");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_USERNAME, "UpdateUserName");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_PASSWORD, "N1Password");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_DEFAULTPLANT, "PLP");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_PLANTCODES, "ANO;PLP");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_EMAIL, "adsad@dsd.dd");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_PHONE, "12321");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_DATEFORMAT, "ddMMyyyy");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_TIMEFORMAT, "HHmmss");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_ENABLED, true);
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_ISLOCALACCOUNT, false);
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_ISADMIN, false);
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_ISPLANTADMIN, false);
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_ISREPORTADMIN, false);
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_ISCONFIGURATOR, false);
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_ISALERTADMIN, false);
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_ISDOCADMIN, false);
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_ISUSER, true);
		response = connectionInterface.update(2321L, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CHANGE_USER_SUCCESS);
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Test
	public void updateUserUserProvisionedWIthOptionalData() throws Exception {
		SyntempoConnectionInterface connectionInterface = new SyntempoConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_LN, "UpdateLN");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_FN, "UpdateFN");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_USERNAME, "UpdateUserName");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_PASSWORD, "Password");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_DEFAULTPLANT, "ANO");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_PLANTCODES, "ANO");
		List roles = new ArrayList();
		roles.add(prepareRoleInformation("HQN"));
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_EMAIL, "asdca@dss.cc");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_ISUSER, true);
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_ENABLED, true);
		IProvisioningResult response = connectionInterface.create(12321L, roles, userParameters, null, null);
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_LN, "");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_FN, "");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_PASSWORD, "");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_DEFAULTPLANT, "RBS");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_PLANTCODES, "RBS");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_EMAIL, "asdsa@dss.cc");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_PHONE, "234234");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_DATEFORMAT, "yyyyMMdd");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_TIMEFORMAT, "HHmmss");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_ENABLED, false);
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_ISLOCALACCOUNT, false);
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_ISADMIN, false);
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_ISPLANTADMIN, false);
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_ISREPORTADMIN, false);
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_ISCONFIGURATOR, false);
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_ISALERTADMIN, false);
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_ISDOCADMIN, false);
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_ISUSER, true);
		response = connectionInterface.update(2321L, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CHANGE_USER_SUCCESS);
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Test
	public void updateUserUserProvisionedError() throws Exception {
		connectionParams.put(SyntempoConnectorConstants.SHOW_PROVISION_WARNINGS, "False");
		SyntempoConnectionInterface connectionInterface = new SyntempoConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_LN, "UpdateLN");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_FN, "UpdateFN");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_USERNAME, "UpdateUserName");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_PASSWORD, "Passw0rd");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_DEFAULTPLANT, "ANO");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_PLANTCODES, "ANO");
		List roles = new ArrayList();
		roles.add(prepareRoleInformation("HQN"));
		roles.add(prepareRoleInformation("PLP"));
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_EMAIL, "asdca@dss.cc");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_ISUSER, true);
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_ENABLED, true);
		IProvisioningResult response = connectionInterface.create(12321L, roles, userParameters, null, null);
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_LN, "UpdateLN updated");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_FN, "UpdateFN updated");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_USERNAME, "UpdateUserName updated");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_PASSWORD, "Passwird");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_DEFAULTPLANT, "PLP");
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_PLANTCODES, "ANO;PLP");
		response = connectionInterface.update(2321L, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CHANGE_USER_FAILURE);
		userParameters.put(SyntempoConnectorConstants.ATTR_USER_USERNAME, "UpdateUserName");
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
	}

	private IRoleInformation prepareRoleInformation(String name) {
		IRoleInformation roleInformation = new RoleInformation();
		roleInformation.setRoleType(IExtractionConstants.ROLE_TYPE_SINGLE);
		roleInformation.setName(name);
		roleInformation.setDescription(name);
		roleInformation.setValidFrom(null);
		roleInformation.setValidFrom(null);
		return roleInformation;
	}
}
